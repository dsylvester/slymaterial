import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule }    from "@angular/forms";
import { ToggleSwitch } from "./Components/ToggleSwitch/toggleSwitch";
import { NotificationComponent } from 
    "./Components/MessageToaster/Notification/NotificationComponent";
import { MessageToasterService } from 
    "./Components/MessageToaster/Services/messageToasterService";
import { MessageToaster } from "./Components/MessageToaster/MessageToaster";
import { MessageMarquee } from "./Components/MessageMarquee/MessageMarquee";
import { MessageMarqueeService } from 
    "./Components/MessageMarquee/Services/MessageMarqueeService";
import { FlyoutMenu, FlyoutMenuService } from "./Components/FlyoutMenu/index";
import { Button } from "./Components/Button/Button";
import { CalendarControl } from "./Components/CalendarControl/CalendarControl";
import { Dialog } from "./Components/Dialog/Dialog";
import { DialogService } from "./Components/Dialog/DialogService";
import { ContactChip } from "./Components/ContactChip/ContactChip";
import { DataGridService } from "./Components/DataGrid/DataGridService";
import { DataGrid } from "./Components/DataGrid/DataGrid";
import { DGHeader } from "./Components/DataGrid/DGHeader";
import { DGDataRow } from "./Components/DataGrid/DGDataRow";
import { DGFooter } from "./Components/DataGrid/DGFooter";
import { Checkbox } from "./Components/Checkbox/Checkbox";
import { ModalService } from "./Components/Modal/ModalService";
import { ActionButton } from "./Components/Button-Action/index";
import { ContentEditable } from "./Components/ContentEditable/index";
import { WindowRef, DesktopNotificationService, GeoCodingService } from "./Services/index";
import { Modal } from "./Components/Modal/index";
// import { RowConfigModel, DataRowType } from "./Components/DataGrid/index";




@NgModule({
    imports: [
         BrowserModule,
         FormsModule,
         CommonModule,
         BrowserAnimationsModule
    ],
    exports:        [   ToggleSwitch, MessageToaster, NotificationComponent,
                        MessageMarquee, FlyoutMenu, Button, CalendarControl, Dialog,
                        ContactChip, DataGrid, DGHeader, DGDataRow, DGFooter, Checkbox,
                        ActionButton, ContentEditable, Modal
                    ],

    declarations:   [   ToggleSwitch, MessageToaster, NotificationComponent,
                        MessageMarquee, FlyoutMenu, Button, CalendarControl, Dialog,
                        ContactChip, DataGrid, DGHeader, DGDataRow, DGFooter, Checkbox,
                        ActionButton, ContentEditable, Modal
                    ],
                    
    providers:      [    ]
})


export class SlyMaterialModule {

    public static forRoot() {
    return {
      ngModule: SlyMaterialModule,
      providers: [ 
          MessageToasterService, MessageMarqueeService, 
          DialogService, DataGridService, FlyoutMenuService, ModalService,
          WindowRef, DesktopNotificationService, GeoCodingService  ]
    };
  }

 }

