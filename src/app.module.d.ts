import { MessageToasterService } from "./Components/MessageToaster/Services/messageToasterService";
import { MessageMarqueeService } from "./Components/MessageMarquee/Services/MessageMarqueeService";
import { FlyoutMenuService } from "./Components/FlyoutMenu/index";
import { DialogService } from "./Components/Dialog/DialogService";
import { DataGridService } from "./Components/DataGrid/DataGridService";
import { ModalService } from "./Components/Modal/ModalService";
import { WindowRef, DesktopNotificationService, GeoCodingService } from "./Services/index";
export declare class SlyMaterialModule {
    static forRoot(): {
        ngModule: typeof SlyMaterialModule;
        providers: (typeof MessageToasterService | typeof MessageMarqueeService | typeof DialogService | typeof DataGridService | typeof FlyoutMenuService | typeof ModalService | typeof WindowRef | typeof DesktopNotificationService | typeof GeoCodingService)[];
    };
}
