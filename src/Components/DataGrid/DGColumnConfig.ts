import { IDGColumnConfig } from "./IDGColumnConfig";

export class DGColumnConfig implements IDGColumnConfig {

    constructor(
        public Name: string,
        public Size: number = 1,
        ) {
            if (this.Name.length === 0) {
                throw new Error("You must provide a Name for DGColumnConfig()");
            }
    }
}
