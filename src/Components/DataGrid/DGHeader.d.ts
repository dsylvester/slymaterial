import { OnInit } from "@angular/core";
import { DataGridHeaderConfig, IDGGridRow } from "./index";
export declare class DGHeader implements OnInit, IDGGridRow {
    HeaderConfig: DataGridHeaderConfig;
    rowSelected: false;
    useSelectAllRows: true;
    constructor();
    ngOnInit(): void;
    onRowSelected(): void;
}
