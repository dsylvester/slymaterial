import { IRowConfigModel } from "./IRowConfigModel";
import { DataRowType } from "./DataRowType";
export declare class RowConfigModel implements IRowConfigModel {
    Name: string;
    Size: number;
    OrdinalName: string;
    RowType: DataRowType;
    constructor(Name?: string, Size?: number, OrdinalName?: string, RowType?: DataRowType);
}
