import { DataGridHeaderConfig } from "./DataGridHeaderConfig";
import { DGDataColumnConfig } from "./DGDataColumnConfig";


export class DataGridConfig {

    //TODO
    // private data: any;

    //  get Data(): any {
    //     return this.data;
    // }

    // set Data(val: any) {
    //     this.data = val;
        
    // }


    constructor(public Data: any | Array<any>,
        public HeaderConfig: DataGridHeaderConfig,
        public DataRowConfig: Array<DGDataColumnConfig>) { }

}





