System.register(["rxjs/Rx"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Rx_1, onDataChange, onDataChanged;
    return {
        setters: [
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            }
        ],
        execute: function () {
            exports_1("onDataChange", onDataChange = Rx_1.Observable.create(function (observer) {
                observer.onNext(42);
            }));
            exports_1("onDataChanged", onDataChanged = new Rx_1.Subject());
        }
    };
});
//# sourceMappingURL=DataGridEvents.js.map