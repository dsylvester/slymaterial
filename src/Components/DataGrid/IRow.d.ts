export interface IRow {
    ID: number | string;
    Selected: boolean;
}
