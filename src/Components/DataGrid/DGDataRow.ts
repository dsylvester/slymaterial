import {
    Component, OnInit, AfterViewInit, AfterContentInit, Input, ViewChild,
    ViewContainerRef, ComponentFactoryResolver, Output
} from "@angular/core";
import { Subject } from "rxjs/Subject";
import { IDGGridRow, DGColumnConfig, DataRowType } from "./index";
import { DataGridService } from "./index";


@Component({
    selector: "sfm-data-grid-row",
    moduleId: __moduleName,
    templateUrl: "./dgDataRow.html",
    styleUrls: ["./dataGrid.css", "DGDataRow.css"]
})


export class DGDataRow implements OnInit, AfterViewInit, AfterContentInit, IDGGridRow {

    @Input() public data: Array<any>;
    @Input() public rowConfig: Array<DGColumnConfig>;
    @Output() public onRowSelected: Subject<any>;

    @ViewChild('customComponent', { read: ViewContainerRef }) public parent;

    public rowSelected = false;
    public useSelectAllRows = false;
    private selectedRows = new Map<number, boolean>();
    public FilteredData: Array<any> = new Array<any>();
    public drt: DataRowType;

    constructor(public dataGridService: DataGridService,
        private resolver: ComponentFactoryResolver) {

            this.onRowSelected = new Subject<any>();

    }

    public ngOnInit(): void {

    }

    public ngAfterViewInit(): void {


        // console.log(`This.rowConfig: ${JSON.stringify(this.rowConfig, null, 5)}`);
        // console.log(`This.Data: ${JSON.stringify(this.data, null, 5)}`);
        console.log(this.data);
        if (this.data !== undefined) {
            setTimeout(() => {
                //  this.populateSelectedRows(this.data);

                this.FilteredData = this.data.map((x) => {
                    return x;
                });

                console.log(this.FilteredData);

            }, 1000);

        }

    }

    public ngAfterContentInit(): void {
        // this.parent.CreateComponent(this.resolver.ComponentFactoryResolver(Chip));
    }

    public SetMaxSize(colNum: number): string {
        return `${(colNum * 10)}%`;

    }

    public RowClicked(idx: number): void {
        this.ToggleRowSelection(idx);

    }

    public ToggleRowSelection(idx: number): void {
        try {

            let currentValue = this.selectedRows.get(idx);
            this.selectedRows.set(idx, (currentValue) ? false : true);

            // console.log(`aaaa: ${this.selectedRows[idx]}`);
        } catch (error) {
            throw new Error("Unable to find provided Index for Selected Row");

        }


    }

    public isRowSelect(idx: number): boolean | undefined {

        let val = this.selectedRows.get(idx);

        if (val === undefined) {
            return false;
            // throw new Error("Can't determine row index");
        }

        return val;

    }

    public CreateComponent(): void {

    }

    private populateSelectedRows(data: Array<any>): void {
        console.log(`data: ${JSON.stringify(data, null, 5)}`);

        if (data === undefined || data.length === 0) {
            return;
        }
        let index = 0;

        data.forEach(() => {
            this.selectedRows.set(index, false);
            index++;
        });

    }

    public RowHasBeenDoubledClick(data: any): void {

        this.onRowSelected.next(data);

    }






}
