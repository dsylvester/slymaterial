import { DGColumnConfig } from "./DGColumnConfig";
import { DGIconTypes } from "./DGCustomTypes";

export class DGDataColumnConfig extends DGColumnConfig {

    constructor(public name: string, public size: number, public format?: DGIconTypes ) {
        super(name, size);


    }

}




