import { OnInit, AfterViewInit } from "@angular/core";
import { DataGridService } from "./DataGridService";
export declare class DGFooter implements OnInit, AfterViewInit {
    dataGridService: DataGridService;
    data: Array<any>;
    constructor(dataGridService: DataGridService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    onRowSelected(): void;
}
