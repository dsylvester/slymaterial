import { DataGridHeaderConfig } from "./DataGridHeaderConfig";
import { DGDataColumnConfig } from "./DGDataColumnConfig";
export declare class DataGridConfig {
    Data: any | Array<any>;
    HeaderConfig: DataGridHeaderConfig;
    DataRowConfig: Array<DGDataColumnConfig>;
    constructor(Data: any | Array<any>, HeaderConfig: DataGridHeaderConfig, DataRowConfig: Array<DGDataColumnConfig>);
}
