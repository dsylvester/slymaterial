import { Component, OnInit, AfterViewInit, Input } from "@angular/core";
import { DataGridService } from "./DataGridService";

@Component({
    selector: "ct-dg-footer",
    moduleId: __moduleName,
    templateUrl: "./dgFooter.html",
    styleUrls: [ "./dataGrid.css", "./dgFooter.css" ]
})


export class DGFooter implements OnInit, AfterViewInit  {

    @Input() public data: Array<any>;


    constructor(public dataGridService: DataGridService) {


    }

    public ngOnInit(): void {


    }

    public ngAfterViewInit(): void {

    }

    public onRowSelected(): void {

    }


}

