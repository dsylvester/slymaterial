import { DataRowType } from "./DataRowType";

export interface IRowConfigModel {

    Name?: string;
    Size?: number;
    OrdinalName?: string;
    RowType: DataRowType;
    

}
