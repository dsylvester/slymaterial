System.register(["./DGColumnConfig"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __moduleName = context_1 && context_1.id;
    var DGColumnConfig_1, DGDataColumnConfig;
    return {
        setters: [
            function (DGColumnConfig_1_1) {
                DGColumnConfig_1 = DGColumnConfig_1_1;
            }
        ],
        execute: function () {
            DGDataColumnConfig = /** @class */ (function (_super) {
                __extends(DGDataColumnConfig, _super);
                function DGDataColumnConfig(name, size, format) {
                    var _this = _super.call(this, name, size) || this;
                    _this.name = name;
                    _this.size = size;
                    _this.format = format;
                    return _this;
                }
                return DGDataColumnConfig;
            }(DGColumnConfig_1.DGColumnConfig));
            exports_1("DGDataColumnConfig", DGDataColumnConfig);
        }
    };
});
//# sourceMappingURL=DGDataColumnConfig.js.map