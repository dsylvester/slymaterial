import { IRowConfigModel } from "./IRowConfigModel";
import { DataRowType } from "./DataRowType";


export class RowConfigModel implements IRowConfigModel {

    constructor(
        public Name?: string,
        public Size?: number,
        public OrdinalName?: string,
        public RowType: DataRowType = DataRowType.HTML
    ) { }
}
