import { DGColumnTypes, DGDataColumnConfig, DGColumnConfig } from "./index";

export class DataGridConfigHelper {

    public static GenerateColumns(cols: Array<DGColumnConfig>, colType: DGColumnTypes = "Data"): 
        Array<DGColumnConfig> | Array<DGDataColumnConfig> {       

        switch (colType.toLocaleLowerCase()) {

            case "data":
                return this.createDataColumns(cols);

            case "header":
                return this.createHeaderColumns(cols);
            default:
                return null;
        }

        
    }


     private static createHeaderColumns(cols: Array<DGColumnConfig>): Array<DGColumnConfig> {
        
        this.emptyArrayCheck(cols, "createDataColumns");

        return cols;
     
    }

    private static createDataColumns(cols: Array<DGColumnConfig>): Array<DGDataColumnConfig> {

        this.emptyArrayCheck(cols, "createDataColumns");

        let result = new Array<DGDataColumnConfig>();

        cols.forEach((x: DGColumnConfig) => {
            result.push(new DGDataColumnConfig(x.Name, x.Size));
        });
        
        return result;
    }

    private static emptyArrayCheck(array: Array<any>, functionName: string): void {
        if (array.length === 0) {
            throw new Error(`You must provide an Array with at least one element in ${functionName}`);
        }
    }


}
