System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DataGridConfig;
    return {
        setters: [],
        execute: function () {
            DataGridConfig = /** @class */ (function () {
                //TODO
                // private data: any;
                //  get Data(): any {
                //     return this.data;
                // }
                // set Data(val: any) {
                //     this.data = val;
                // }
                function DataGridConfig(Data, HeaderConfig, DataRowConfig) {
                    this.Data = Data;
                    this.HeaderConfig = HeaderConfig;
                    this.DataRowConfig = DataRowConfig;
                }
                return DataGridConfig;
            }());
            exports_1("DataGridConfig", DataGridConfig);
        }
    };
});
//# sourceMappingURL=DataGridConfig.js.map