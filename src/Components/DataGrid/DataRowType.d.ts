export declare enum DataRowType {
    HTML = 0,
    COMPONENT = 1,
    INLINE = 2,
}
