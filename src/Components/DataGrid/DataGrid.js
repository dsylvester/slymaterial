System.register(["@angular/core", "./DataGridService", "rxjs/Subject"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, DataGridService_1, Subject_1, DataGrid;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (DataGridService_1_1) {
                DataGridService_1 = DataGridService_1_1;
            },
            function (Subject_1_1) {
                Subject_1 = Subject_1_1;
            }
        ],
        execute: function () {
            DataGrid = /** @class */ (function () {
                function DataGrid(dataGridService) {
                    // private toaster: MessageToasterService
                    this.dataGridService = dataGridService;
                    this.onRowSelection = new Subject_1.Subject();
                    // public header: DataGridHeaderConfig;
                    this.onItemPerPageChange = new core_1.EventEmitter();
                    this.onPageChange = new core_1.EventEmitter();
                    this.maxItemsPerList = 10;
                    this.MaxPagingPages = 0;
                    this.CurrentPagingPageNumber = 1;
                    this.Consultants = [];
                    this.ListOfItemsPerPage = new Array();
                    this.DataGridList = new Array();
                    (_a = this.ListOfItemsPerPage).push.apply(_a, [{ value: 5 }, { value: 10 },
                        { value: 20 }, { value: 25 }, { value: 30 }, { value: 50 }]);
                    var _a;
                }
                // public ToLeadSummary(id) {
                //     this._router.navigateByUrl(`/lead/summary/${id}`);
                // }
                DataGrid.prototype.ngOnInit = function () {
                };
                DataGrid.prototype.ngAfterViewInit = function () {
                    var _this = this;
                    console.log(this.data);
                    console.log(this.header);
                    setTimeout(function () {
                        _this.dataGridService.CompleteData = _this.data;
                    }, 1000);
                };
                DataGrid.prototype.onRowSelected = function (evt) {
                    this.dataGridService.onRowSelected(evt);
                };
                DataGrid.prototype.onRowDoubleClicked = function (evt) {
                    this.onRowSelection.next(evt);
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Array)
                ], DataGrid.prototype, "data", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Array)
                ], DataGrid.prototype, "header", void 0);
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", Object)
                ], DataGrid.prototype, "onRowSelection", void 0);
                DataGrid = __decorate([
                    core_1.Component({
                        selector: "sfm-data-grid",
                        moduleId: __moduleName,
                        templateUrl: "./DataGrid.html",
                        styleUrls: ["./DataGrid.css"]
                    }),
                    __metadata("design:paramtypes", [DataGridService_1.DataGridService])
                ], DataGrid);
                return DataGrid;
            }());
            exports_1("DataGrid", DataGrid);
        }
    };
});
//# sourceMappingURL=DataGrid.js.map