import { OnInit, AfterViewInit, AfterContentInit, ComponentFactoryResolver } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { IDGGridRow, DGColumnConfig, DataRowType } from "./index";
import { DataGridService } from "./index";
export declare class DGDataRow implements OnInit, AfterViewInit, AfterContentInit, IDGGridRow {
    dataGridService: DataGridService;
    private resolver;
    data: Array<any>;
    rowConfig: Array<DGColumnConfig>;
    onRowSelected: Subject<any>;
    parent: any;
    rowSelected: boolean;
    useSelectAllRows: boolean;
    private selectedRows;
    FilteredData: Array<any>;
    drt: DataRowType;
    constructor(dataGridService: DataGridService, resolver: ComponentFactoryResolver);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngAfterContentInit(): void;
    SetMaxSize(colNum: number): string;
    RowClicked(idx: number): void;
    ToggleRowSelection(idx: number): void;
    isRowSelect(idx: number): boolean | undefined;
    CreateComponent(): void;
    private populateSelectedRows(data);
    RowHasBeenDoubledClick(data: any): void;
}
