import { Component, OnInit, Input } from "@angular/core";
import { DataGridHeaderConfig, IDGGridRow } from "./index";

@Component({
    selector: "sfm-data-grid-header",
    moduleId: __moduleName,
    templateUrl: "./dgHeader.html",
    styleUrls: [ "./dataGrid.css", "./dgHeader.css" ]
})


export class DGHeader implements OnInit, IDGGridRow  {

    @Input() public HeaderConfig: DataGridHeaderConfig;


    public rowSelected: false;
    public useSelectAllRows: true;

    constructor() {

    }


    public ngOnInit() {


    }

    public onRowSelected(): void {

    }


}
