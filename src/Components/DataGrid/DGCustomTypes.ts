export type DGColumnTypes =  "Header" | "Data";

export type DGIconTypes = "Font-Awesome" | "Flat-Icon" | "Html";

