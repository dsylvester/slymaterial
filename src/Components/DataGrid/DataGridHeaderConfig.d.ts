import { DGColumnConfig } from "./DGColumnConfig";
export declare class DataGridHeaderConfig {
    Columns: Array<DGColumnConfig>;
    constructor(Columns: Array<DGColumnConfig>);
}
