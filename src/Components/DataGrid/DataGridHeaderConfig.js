System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DataGridHeaderConfig;
    return {
        setters: [],
        execute: function () {
            DataGridHeaderConfig = /** @class */ (function () {
                function DataGridHeaderConfig(Columns) {
                    this.Columns = Columns;
                }
                return DataGridHeaderConfig;
            }());
            exports_1("DataGridHeaderConfig", DataGridHeaderConfig);
        }
    };
});
//# sourceMappingURL=DataGridHeaderConfig.js.map