import { DGColumnTypes, DGDataColumnConfig, DGColumnConfig } from "./index";
export declare class DataGridConfigHelper {
    static GenerateColumns(cols: Array<DGColumnConfig>, colType?: DGColumnTypes): Array<DGColumnConfig> | Array<DGDataColumnConfig>;
    private static createHeaderColumns(cols);
    private static createDataColumns(cols);
    private static emptyArrayCheck(array, functionName);
}
