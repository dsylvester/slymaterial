import { OnInit, EventEmitter, AfterViewInit } from "@angular/core";
import { DataGridService } from "./DataGridService";
import { RowConfigModel } from "./RowConfigModel";
import { Subject } from "rxjs/Subject";
export declare class DataGrid implements OnInit, AfterViewInit {
    dataGridService: DataGridService;
    /**
     * @param {DataGridConfig } -
     * The configuration Object
     * for the Data Grid
     */
    data: Array<any>;
    header: Array<RowConfigModel>;
    onRowSelection: Subject<any>;
    onItemPerPageChange: EventEmitter<number>;
    onPageChange: EventEmitter<number>;
    maxItemsPerList: number;
    PagingPages: Array<number>;
    MaxPagingPages: number;
    CurrentPagingPageNumber: number;
    Consultants: any[];
    ListOfItemsPerPage: Array<any>;
    DataGridList: Array<any>;
    constructor(dataGridService: DataGridService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    onRowSelected(evt: any): void;
    onRowDoubleClicked(evt: any): void;
}
