import { DGColumnConfig } from "./DGColumnConfig";
import { DGIconTypes } from "./DGCustomTypes";
export declare class DGDataColumnConfig extends DGColumnConfig {
    name: string;
    size: number;
    format: DGIconTypes;
    constructor(name: string, size: number, format?: DGIconTypes);
}
