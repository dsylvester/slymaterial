System.register(["./DataGridHeaderConfig", "./DGHeader", "./DGFooter", "./DataGrid", "./DGColumnConfig", "./DGDataRow", "./DataGridConfig", "./DGDataColumnConfig", "./DataGridService", "./RowConfigModel", "./DataRowType"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (DataGridHeaderConfig_1_1) {
                exportStar_1(DataGridHeaderConfig_1_1);
            },
            function (DGHeader_1_1) {
                exportStar_1(DGHeader_1_1);
            },
            function (DGFooter_1_1) {
                exportStar_1(DGFooter_1_1);
            },
            function (DataGrid_1_1) {
                exportStar_1(DataGrid_1_1);
            },
            function (DGColumnConfig_1_1) {
                exportStar_1(DGColumnConfig_1_1);
            },
            function (DGDataRow_1_1) {
                exportStar_1(DGDataRow_1_1);
            },
            function (DataGridConfig_1_1) {
                exportStar_1(DataGridConfig_1_1);
            },
            function (DGDataColumnConfig_1_1) {
                exportStar_1(DGDataColumnConfig_1_1);
            },
            function (DataGridService_1_1) {
                exportStar_1(DataGridService_1_1);
            },
            function (RowConfigModel_1_1) {
                exportStar_1(RowConfigModel_1_1);
            },
            function (DataRowType_1_1) {
                exportStar_1(DataRowType_1_1);
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map