System.register(["@angular/core", "rxjs", "./DGColumnConfig", "./DGDataColumnConfig", "./DataGridHeaderConfig"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, rxjs_1, DGColumnConfig_1, DGDataColumnConfig_1, DataGridHeaderConfig_1, DataGridService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (rxjs_1_1) {
                rxjs_1 = rxjs_1_1;
            },
            function (DGColumnConfig_1_1) {
                DGColumnConfig_1 = DGColumnConfig_1_1;
            },
            function (DGDataColumnConfig_1_1) {
                DGDataColumnConfig_1 = DGDataColumnConfig_1_1;
            },
            function (DataGridHeaderConfig_1_1) {
                DataGridHeaderConfig_1 = DataGridHeaderConfig_1_1;
            }
        ],
        execute: function () {
            DataGridService = /** @class */ (function () {
                function DataGridService() {
                    // this.GridConfig = new DataGridConfig(null, null, null);
                    // console.log("GridConfig: " + JSON.stringify(this.GridConfig, null, 5));
                    var _this = this;
                    this.FilteredData = new Array();
                    this.selectedItems = new Array();
                    // public GridConfig: DataGridConfig = new DataGridConfig(null, null, null);
                    this.FilteredData$ = new rxjs_1.Subject();
                    this.maxItemsPerList = 10;
                    this.MaxPagingPages = 0;
                    this.CurrentPagingPageNumber = 1;
                    this.ListOfItemsPerPage = new Array();
                    this.onItemPerPageChange = new rxjs_1.Subject();
                    this.onPageChange = new rxjs_1.Subject();
                    this.onDataChanged = new rxjs_1.Subject();
                    this.onSelectionChange = new rxjs_1.Subject();
                    this.rowSelected = false;
                    this.onDataChanged.subscribe(function (data) {
                        // this.GridConfig.Data = data;
                        console.log("Grid COnfig");
                        console.log(_this.GridConfig);
                    });
                    this.PopulatePagingInitialValues();
                    this.onPageChange.subscribe(function (pageNo) {
                        _this.CurrentPagingPageNumber = pageNo;
                        _this.PopulateDataGridList();
                    });
                    this.onRowSelection = new rxjs_1.Subject();
                    this.ItemPerPageChange();
                }
                Object.defineProperty(DataGridService.prototype, "GridConfig", {
                    get: function () {
                        return this.gridConfig;
                    },
                    set: function (val) {
                        this.gridConfig = val;
                        if (val.Data !== undefined || val.Data !== null) {
                            this.PopulateDataGridList();
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(DataGridService.prototype, "CompleteData", {
                    get: function () {
                        return this.completeData;
                    },
                    set: function (val) {
                        this.completeData = val;
                        console.log(this.completeData);
                        if (this.completeData === undefined || this.completeData === null) {
                            // this.FilteredData = this.completeData;
                            this.FilteredData$.next(this.completeData);
                        }
                        else {
                            // this.FilteredData = this.completeData;
                            this.FilteredData$.next(this.completeData);
                        }
                        // if (val.Data !== undefined || val.Data !== null) {
                        //     this.PopulateDataGridList();
                        // }
                    },
                    enumerable: true,
                    configurable: true
                });
                DataGridService.prototype.Configuration = function (def, data) {
                    this.generateGridConfiguration(def);
                    this.generateData(data);
                    // this.CompleteData = data;
                    this.PopulateDataGridList();
                };
                DataGridService.prototype.dataChanged = function (data) {
                    this.onDataChanged.next(data);
                    // this.onDataChanged.next(this.FilteredData);        
                };
                DataGridService.prototype.ItemPerPageChange = function () {
                    this.PagingPages = this.DeterminePaging();
                    this.PopulateDataGridList();
                    this.onItemPerPageChange.next(this.maxItemsPerList);
                };
                DataGridService.prototype.DeterminePaging = function () {
                    if (this.CompleteData === undefined || this.CompleteData === null) {
                        return null;
                    }
                    var maxPages = Math.ceil(this.CompleteData.length / this.maxItemsPerList);
                    var result = new Array();
                    if (maxPages > 1) {
                        for (var i = 1; i <= maxPages; i++) {
                            result.push(i);
                        }
                    }
                    else {
                        result.push(1);
                    }
                    this.MaxPagingPages = result.length;
                    return result;
                };
                DataGridService.prototype.addSelectedRowToSelectionsArray = function (row) {
                    if (!this.doesRowExist(row)) {
                        this.selectedItems.push(row);
                        this.onSelectionChange.next(this.selectedItems);
                    }
                    else {
                        this.removeSelectedRowToSelectionsArray(row);
                        this.addSelectedRowToSelectionsArray(row);
                    }
                };
                DataGridService.prototype.removeSelectedRowToSelectionsArray = function (row) {
                    var index = -1;
                    this.selectedItems.some(function (tl, idx) {
                        if (tl.ID === row.ID) {
                            index = idx;
                            return true;
                        }
                        else {
                            return false;
                        }
                    });
                    if (index > -1) {
                        this.onSelectionChange.next(this.selectedItems);
                        this.selectedItems.splice(index, 1);
                    }
                };
                DataGridService.prototype.doesRowExist = function (row) {
                    return this.selectedItems.some(function (x) {
                        return (x.ID === row.ID);
                    });
                };
                DataGridService.prototype.generateGridConfiguration = function (def) {
                    var headers = new Array();
                    var dataRows = Array();
                    def.map(function (x) {
                        headers.push(new DGColumnConfig_1.DGColumnConfig(x[0], x[1]));
                        dataRows.push(new DGDataColumnConfig_1.DGDataColumnConfig(x[2], x[1], (x[3] === undefined) ? "" : x[3]));
                    });
                    var header = new DataGridHeaderConfig_1.DataGridHeaderConfig(headers);
                    this.GridConfig.HeaderConfig = header;
                    this.GridConfig.DataRowConfig = dataRows;
                };
                DataGridService.prototype.generateData = function (data) {
                    this.GridConfig.Data = data;
                };
                DataGridService.prototype.GetItemsFromMasterList = function (startingIndex, numberOfItemsToGrab) {
                    // Ensures the stopIndex isn't greater than the Leads total items in array
                    var stopIndex = (Number(numberOfItemsToGrab) + startingIndex);
                    stopIndex = (stopIndex > this.CompleteData.length) ? this.CompleteData.length : stopIndex;
                    return this.CompleteData.filter(function (x, index) {
                        return (index >= startingIndex && index < stopIndex);
                    });
                };
                DataGridService.prototype.PopulatePagingInitialValues = function () {
                    (_a = this.ListOfItemsPerPage).push.apply(_a, [{ value: 5 }, { value: 10 }, { value: 20 }, { value: 25 }, { value: 30 }, { value: 50 }]);
                    var _a;
                };
                DataGridService.prototype.PopulateDataGridList = function () {
                    // this.FilteredData = new Array<any>();
                    // if (this.CompleteData !== undefined || this.CompleteData !== null) {
                    //     // Populate Active DataGrid List
                    //     if (this.CompleteData.length <= this.maxItemsPerList) {
                    //         // this.FilteredData.push(...this.CompleteData);
                    //         // this.onDataChanged.next(this.FilteredData);
                    //     }
                    //     else {
                    //         // Get amount of data to display 
                    //         let pageNumber = (this.CurrentPagingPageNumber - 1) * 1;
                    //         let startingIndex = (pageNumber * this.maxItemsPerList);
                    //         this.FilteredData = this.GetItemsFromMasterList(startingIndex, this.maxItemsPerList);
                    //         this.onDataChanged.next(this.FilteredData);
                    //         // start count 10
                    //         // 11 -19;pagenumber * items (1 * 10) =  10 start count 10
                    //         // 20 - 29;pagenumber * items (2 * 10) =  20 start count 10
                    //     }
                    // }
                };
                DataGridService.prototype.LoadNext = function (pageNumber) {
                    this.CurrentPagingPageNumber = pageNumber;
                    this.PopulateDataGridList();
                };
                DataGridService.prototype.DisableNavigationButton = function () {
                    if (this.MaxPagingPages === 1) {
                        return true;
                    }
                    return null;
                };
                DataGridService.prototype.DisableItemsPerPage = function () {
                    if (this.CompleteData.length < this.maxItemsPerList) {
                        return true;
                    }
                    return null;
                };
                DataGridService.prototype.ToFirstPage = function () {
                    this.CurrentPagingPageNumber = 1;
                    this.onPageChange.next(this.CurrentPagingPageNumber);
                };
                DataGridService.prototype.ToNextPage = function () {
                    var nextPage = this.CurrentPagingPageNumber + 1;
                    if (nextPage > this.MaxPagingPages) {
                        this.CurrentPagingPageNumber = this.MaxPagingPages;
                    }
                    else {
                        this.CurrentPagingPageNumber = nextPage;
                    }
                    this.onPageChange.next(this.CurrentPagingPageNumber);
                };
                DataGridService.prototype.ToPreviousPage = function () {
                    var nextPage = this.CurrentPagingPageNumber - 1;
                    if (nextPage < 1) {
                        this.CurrentPagingPageNumber = 1;
                    }
                    else {
                        this.CurrentPagingPageNumber = nextPage;
                    }
                    this.onPageChange.next(this.CurrentPagingPageNumber);
                };
                DataGridService.prototype.ToLastPage = function () {
                    this.CurrentPagingPageNumber = this.MaxPagingPages;
                    this.onPageChange.next(this.CurrentPagingPageNumber);
                };
                DataGridService.prototype.onRowSelected = function (row) {
                    row.Selected = (row.Selected === false) ? true : false;
                    if (row.Selected) {
                        //  Add selection to Array of Selected Items
                        this.addSelectedRowToSelectionsArray(row);
                    }
                    else {
                        this.removeSelectedRowToSelectionsArray(row);
                    }
                };
                DataGridService = __decorate([
                    core_1.Injectable()
                    /**
                     * Provide access to the className
                     * Service web API
                     *
                     */
                    ,
                    __metadata("design:paramtypes", [])
                ], DataGridService);
                return DataGridService;
            }());
            exports_1("DataGridService", DataGridService);
        }
    };
});
//# sourceMappingURL=DataGridService.js.map