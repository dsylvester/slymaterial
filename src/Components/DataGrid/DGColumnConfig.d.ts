import { IDGColumnConfig } from "./IDGColumnConfig";
export declare class DGColumnConfig implements IDGColumnConfig {
    Name: string;
    Size: number;
    constructor(Name: string, Size?: number);
}
