System.register(["./index"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var index_1, DataGridConfigHelper;
    return {
        setters: [
            function (index_1_1) {
                index_1 = index_1_1;
            }
        ],
        execute: function () {
            DataGridConfigHelper = /** @class */ (function () {
                function DataGridConfigHelper() {
                }
                DataGridConfigHelper.GenerateColumns = function (cols, colType) {
                    if (colType === void 0) { colType = "Data"; }
                    switch (colType.toLocaleLowerCase()) {
                        case "data":
                            return this.createDataColumns(cols);
                        case "header":
                            return this.createHeaderColumns(cols);
                        default:
                            return null;
                    }
                };
                DataGridConfigHelper.createHeaderColumns = function (cols) {
                    this.emptyArrayCheck(cols, "createDataColumns");
                    return cols;
                };
                DataGridConfigHelper.createDataColumns = function (cols) {
                    this.emptyArrayCheck(cols, "createDataColumns");
                    var result = new Array();
                    cols.forEach(function (x) {
                        result.push(new index_1.DGDataColumnConfig(x.Name, x.Size));
                    });
                    return result;
                };
                DataGridConfigHelper.emptyArrayCheck = function (array, functionName) {
                    if (array.length === 0) {
                        throw new Error("You must provide an Array with at least one element in " + functionName);
                    }
                };
                return DataGridConfigHelper;
            }());
            exports_1("DataGridConfigHelper", DataGridConfigHelper);
        }
    };
});
//# sourceMappingURL=DataGridConfigHelper.js.map