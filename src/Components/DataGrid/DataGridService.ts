import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { IRow } from "./IRow";
import { DGColumnConfig } from "./DGColumnConfig";
import { DGDataColumnConfig } from "./DGDataColumnConfig";
import { DataGridConfig } from "./DataGridConfig";
import { DataGridHeaderConfig } from "./DataGridHeaderConfig";

@Injectable()
/**
 * Provide access to the className
 * Service web API
 *
 */
export class DataGridService {

    private completeData: Array<any>;
    public FilteredData: Array<any> = new Array<any>();

    private gridConfig: DataGridConfig;

     get GridConfig(): DataGridConfig {
        return this.gridConfig;
    }

    set GridConfig(val: DataGridConfig) {
        this.gridConfig = val;

        if (val.Data !== undefined || val.Data !== null) {
            this.PopulateDataGridList();
        }
        
    }

    get CompleteData(): Array<any> {
        return this.completeData;
    }

    set CompleteData(val: Array<any>) {
        this.completeData = val;

        console.log(this.completeData);


        if (this.completeData === undefined || this.completeData === null) {
            
            
            // this.FilteredData = this.completeData;

            this.FilteredData$.next(this.completeData);
        }
        else {
            // this.FilteredData = this.completeData;
            this.FilteredData$.next(this.completeData);
        }

        // if (val.Data !== undefined || val.Data !== null) {
        //     this.PopulateDataGridList();
        // }
        
    }

        

    private selectedItems: Array<any> = new Array<any>();
    
    // public GridConfig: DataGridConfig = new DataGridConfig(null, null, null);
    
    public FilteredData$: Subject<Array<any>> = new Subject<Array<any>>();

    public maxItemsPerList = 10;
    public PagingPages: Array<number>;
    public MaxPagingPages = 0;
    public CurrentPagingPageNumber = 1;
    public ListOfItemsPerPage: Array<any> = new Array<any>();
    

    public onItemPerPageChange: Subject<number> = new Subject<number>();
    public onPageChange: Subject<number> = new Subject<number>();
    public onDataChanged = new Subject<Array<any>>();
    public onSelectionChange = new Subject<Array<any>>();
    

    public rowSelected =  false;

    
    constructor() {
        // this.GridConfig = new DataGridConfig(null, null, null);
        // console.log("GridConfig: " + JSON.stringify(this.GridConfig, null, 5));
        
        this.onDataChanged.subscribe((data) => {
            // this.GridConfig.Data = data;
            console.log("Grid COnfig");
            console.log(this.GridConfig);
        });
        this.PopulatePagingInitialValues();

        this.onPageChange.subscribe((pageNo: number) => {
            this.CurrentPagingPageNumber = pageNo;
            this.PopulateDataGridList();
        });

        this.onRowSelection = new Subject<any>();

        this.ItemPerPageChange();
    }

   

    public Configuration(def: Array<Array<any>>, data: Array<any>): void {
        this.generateGridConfiguration(def);
        this.generateData(data);

        // this.CompleteData = data;
        
        this.PopulateDataGridList();


    }

    public dataChanged(data: Array<any>): void {
        this.onDataChanged.next(data);  
        // this.onDataChanged.next(this.FilteredData);        
    }

    public ItemPerPageChange(): void {
        
        this.PagingPages = this.DeterminePaging();
        this.PopulateDataGridList();


        this.onItemPerPageChange.next(this.maxItemsPerList);
    }


    private DeterminePaging(): Array<number> {

        if (this.CompleteData === undefined || this.CompleteData === null) {
            return null;
        }

        let maxPages = Math.ceil(this.CompleteData.length / this.maxItemsPerList);

        let result = new Array<number>();

        if (maxPages > 1) {
            for (let i = 1; i <= maxPages; i++) {
                result.push(i);
            }
        } else {
            result.push(1);
        }

        this.MaxPagingPages = result.length;

        return result;
    }

    private addSelectedRowToSelectionsArray(row: IRow) {
        if (!this.doesRowExist(row)) {
            this.selectedItems.push(row);
            this.onSelectionChange.next(this.selectedItems);
        } else {
            this.removeSelectedRowToSelectionsArray(row);
            this.addSelectedRowToSelectionsArray(row);
        }

    }

    private removeSelectedRowToSelectionsArray(row: IRow): void {
        let index = -1;

        this.selectedItems.some((tl: IRow, idx: number) => {
            if (tl.ID === row.ID) {
                index = idx;
                return true;
            } else {
                return false;
            }
        });

        if (index > -1) {

            this.onSelectionChange.next(this.selectedItems);
            this.selectedItems.splice(index, 1);
        }
    }

    private doesRowExist(row: IRow): boolean {
        return this.selectedItems.some((x) => {
            return (x.ID === row.ID);
        });
    }

    private generateGridConfiguration(def: Array<Array<any>>): void {
        let headers = new Array<DGColumnConfig>();
        let dataRows = Array<DGDataColumnConfig>();

        def.map((x) => {
            headers.push(new DGColumnConfig(x[0], x[1]));
            dataRows.push(new DGDataColumnConfig(x[2], x[1], (x[3] === undefined) ? "" : x[3]));

        });

        let header = new DataGridHeaderConfig(headers);

        this.GridConfig.HeaderConfig = header;
        this.GridConfig.DataRowConfig = dataRows;

    }

    private generateData(data: Array<any>): void {
        this.GridConfig.Data = data;
    }

    protected GetItemsFromMasterList(startingIndex: number, numberOfItemsToGrab: number) {
        // Ensures the stopIndex isn't greater than the Leads total items in array
        let stopIndex = (Number(numberOfItemsToGrab) + startingIndex);
        stopIndex = (stopIndex > this.CompleteData.length) ? this.CompleteData.length : stopIndex;

        return this.CompleteData.filter((x, index) => {
            return (index >= startingIndex && index < stopIndex);
        });

    }

    private PopulatePagingInitialValues(): void {
        this.ListOfItemsPerPage.push(...[{ value: 5 }, { value: 10 }, { value: 20 }, { value: 25 }, { value: 30 }, { value: 50 }]);
    }

    private PopulateDataGridList(): void {

        // this.FilteredData = new Array<any>();

        // if (this.CompleteData !== undefined || this.CompleteData !== null) {
        //     // Populate Active DataGrid List
        //     if (this.CompleteData.length <= this.maxItemsPerList) {
        //         // this.FilteredData.push(...this.CompleteData);
        //         // this.onDataChanged.next(this.FilteredData);
                
        //     }
        //     else {
        //         // Get amount of data to display 
        //         let pageNumber = (this.CurrentPagingPageNumber - 1) * 1;
        //         let startingIndex = (pageNumber * this.maxItemsPerList);

        //         this.FilteredData = this.GetItemsFromMasterList(startingIndex, this.maxItemsPerList);
        //         this.onDataChanged.next(this.FilteredData);

        //         // start count 10
        //         // 11 -19;pagenumber * items (1 * 10) =  10 start count 10
        //         // 20 - 29;pagenumber * items (2 * 10) =  20 start count 10

        //     }
        // }
    }

    public LoadNext(pageNumber: number): void {
        this.CurrentPagingPageNumber = pageNumber;
        this.PopulateDataGridList();
    }

    public DisableNavigationButton(): boolean | string | null {
        if (this.MaxPagingPages === 1) {
            return true;
        }
        return null;
    }

    public DisableItemsPerPage(): boolean | string | null {
        if (this.CompleteData.length < this.maxItemsPerList) {
            return true;
        }
        return null;
    }

    public ToFirstPage(): void {
        this.CurrentPagingPageNumber = 1;
        this.onPageChange.next(this.CurrentPagingPageNumber);
    }

    public ToNextPage(): void {
        
        let nextPage = this.CurrentPagingPageNumber + 1;

        if (nextPage > this.MaxPagingPages) {
            this.CurrentPagingPageNumber = this.MaxPagingPages;
        }
        else {
            this.CurrentPagingPageNumber = nextPage;
        }
        
        this.onPageChange.next(this.CurrentPagingPageNumber);
        
    }

    public ToPreviousPage(): void {
        let nextPage = this.CurrentPagingPageNumber - 1;

        if (nextPage < 1) {
            this.CurrentPagingPageNumber = 1;
        }
        else {
            this.CurrentPagingPageNumber = nextPage;
        }
        this.onPageChange.next(this.CurrentPagingPageNumber);
    }

    public ToLastPage(): void {
        this.CurrentPagingPageNumber = this.MaxPagingPages;
        this.onPageChange.next(this.CurrentPagingPageNumber);
    }

    
    
    public onRowSelected(row: IRow): void {

        row.Selected = (row.Selected === false) ? true : false;

        if (row.Selected) {
            //  Add selection to Array of Selected Items
            this.addSelectedRowToSelectionsArray(row);

        } else {
            this.removeSelectedRowToSelectionsArray(row);
        }

    }

    

    
}
