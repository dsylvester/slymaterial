import { Subject } from "rxjs/Subject";

export interface IDGGridRow {

    rowSelected: boolean;

    onRowSelected: Subject<any>;

}
