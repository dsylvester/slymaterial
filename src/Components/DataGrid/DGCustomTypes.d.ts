export declare type DGColumnTypes = "Header" | "Data";
export declare type DGIconTypes = "Font-Awesome" | "Flat-Icon" | "Html";
