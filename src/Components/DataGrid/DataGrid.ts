import { Component, OnInit, EventEmitter, AfterViewInit, Input,
    Output } from "@angular/core";
import { DataGridService } from "./DataGridService";
import { DataGridHeaderConfig } from "./DataGridHeaderConfig";
import { DGColumnConfig } from "./DGColumnConfig";
import { RowConfigModel } from "./RowConfigModel";
import { Subject } from "rxjs/Subject";

@Component({
    selector: "sfm-data-grid",
    moduleId: __moduleName,
    templateUrl: "./DataGrid.html",
    styleUrls: ["./DataGrid.css"]
})



export class DataGrid implements OnInit, AfterViewInit {

    


    /**
     * @param {DataGridConfig } - 
     * The configuration Object 
     * for the Data Grid
     */
    // @Input() public config: DataGridConfig;
    // public config: DataGridConfig;


    @Input() public data: Array<any>;
    @Input() public header: Array<RowConfigModel>;

    @Output() public onRowSelection = new Subject<any>();

    // public header: DataGridHeaderConfig;
    public onItemPerPageChange: EventEmitter<number> = new EventEmitter<number>();
    public onPageChange: EventEmitter<number> = new EventEmitter<number>();

    public maxItemsPerList = 10;
    public PagingPages: Array<number>;
    public MaxPagingPages = 0;
    public CurrentPagingPageNumber = 1;
    public Consultants = [];
    public ListOfItemsPerPage: Array<any> = new Array<any>();
    public DataGridList: Array<any> = new Array<any>();

    
    constructor(public dataGridService: DataGridService ) {
        // private toaster: MessageToasterService
            
        this.ListOfItemsPerPage.push(...[{ value: 5 }, { value: 10 },
            { value: 20 }, { value: 25 }, { value: 30 }, { value: 50 }]);
        
    }


    // public ToLeadSummary(id) {
    //     this._router.navigateByUrl(`/lead/summary/${id}`);
    // }


    public ngOnInit(): void {
      
    }

    public ngAfterViewInit(): void {

        console.log(this.data);
        console.log(this.header);

        setTimeout(() => {
            this.dataGridService.CompleteData = this.data;
        }, 1000);
        
    }

    public onRowSelected(evt: any): void {

        this.dataGridService.onRowSelected(evt);
    }

    public onRowDoubleClicked(evt: any): void {
        this.onRowSelection.next(evt);
    }

    
}
