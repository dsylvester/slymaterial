import { Observable, Subject } from "rxjs/Rx";

export const onDataChange = Observable.create((observer) => {
    observer.onNext(42);
});


export const onDataChanged = new Subject();

