export interface ICardData {

    Name: string;
    Email: string;
    Photo: string;
}
