import { Directive, ElementRef, Renderer, 
    Input, Output, EventEmitter, HostListener,
    HostBinding } from "@angular/core";
// import { LeadModel } from "../../Lead/index";
import { ContentEditableResult } from "./ContentEditableResult";

@Directive({
    selector: "[sfm-editable]"
})


export class ContentEditable {

    @Output() public ContentEdited: EventEmitter<ContentEditableResult>
        = new EventEmitter<ContentEditableResult>();
    @Input() public model: any;
    @Input() public mpn: string;
    @HostListener('dblclick') public onDoubleClick(): void {

        console.log('I was double clicked');
        // this.isEditable = (this.isEditable) ? false : true;
        console.log(`isEditable: ${this.isEditable}`);

    }

    @HostBinding('class.editable-text') private isEditable = false;
    @HostListener('blur') public onBlur(): void {

        console.log('I have been Lost Focus');
        this.isEditable = false;
    }

    @HostListener('focus') public onFocus(): void {

        console.log('I have been focused');
        this.isEditable = true;
    }

    /**
    * Input for Determining which Service to
    * to use to save the Data.    
    */
    @Input() public serviceName: string;

    private hasValueChanged: boolean = false;
    private value: string;


    constructor(private el: ElementRef, 
        private renderer: Renderer) {

    }




    public ngOnInit() {
        this.CreateEventsForAttribute();

    }

    private CreateEventsForAttribute() {
        this.CreateContentEditableEnabled();
        this.CreateContentEditableDisabled();
    }

    private CreateContentEditableEnabled() {
        this.renderer.listen(this.el.nativeElement, 'click', () => {
            // console.log("Entering Edit MOde");
            this.value = this.el.nativeElement.innerText;
            this.renderer.setElementAttribute(this.el.nativeElement, "contenteditable", "true");
            // console.table(<LeadModel>this.model);
            // console.log("Lead - Contact:  " + JSON.stringify(this.model.Contact));

        });
    }

    /**
     * Disables editing on Element and when blur occurs
     * @example <caption>Example usage of method1.</caption>
     * // returns 2
     * globalNS.method1(5, 10);
     * @returns {Number} Returns the value of x for the equation.
     */
    private CreateContentEditableDisabled() {
        this.renderer.listen(this.el.nativeElement, 'blur', () => {
            this.renderer.setElementAttribute(this.el.nativeElement, "contenteditable", "false");

            let modifiedValue = this.el.nativeElement.innerText;
            console.log(`${this.value} === ${modifiedValue}`);
            this.hasValueChanged = (this.value === modifiedValue) ? false : true;

            console.log(`Model: ${JSON.stringify(this.model)}`);
            console.log(`Modified Value: ${modifiedValue}`);

            if (this.hasValueChanged) {
                this.model[this.mpn] = this.el.nativeElement.innerText;
                console.log("Revised: Lead - Contact:  " + JSON.stringify(this.model));

                // this.cds.Save(this.serviceName, this.model);

                this.ContentEdited.emit(new ContentEditableResult(this, modifiedValue, this.mpn));

            } else if (this.serviceName === "contactDetail") {
                // this.cds.Save(this.serviceName, this.model);

                this.ContentEdited.emit(new ContentEditableResult(this, modifiedValue, this.mpn));
            }

        });
    }

}
