System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ContentEditableResult;
    return {
        setters: [],
        execute: function () {
            ContentEditableResult = /** @class */ (function () {
                function ContentEditableResult(Sender, Value, Property) {
                    this.Sender = Sender;
                    this.Value = Value;
                    this.Property = Property;
                }
                return ContentEditableResult;
            }());
            exports_1("ContentEditableResult", ContentEditableResult);
        }
    };
});
//# sourceMappingURL=ContentEditableResult.js.map