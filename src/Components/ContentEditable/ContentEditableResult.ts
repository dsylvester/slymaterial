export class ContentEditableResult {

     constructor(
            public Sender: any,
            public Value: any,
            public Property: string) {

     }
}
