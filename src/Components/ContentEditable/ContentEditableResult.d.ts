export declare class ContentEditableResult {
    Sender: any;
    Value: any;
    Property: string;
    constructor(Sender: any, Value: any, Property: string);
}
