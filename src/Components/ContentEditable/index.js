System.register(["./ContentEditable", "./ContentEditableResult"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (ContentEditable_1_1) {
                exports_1({
                    "ContentEditable": ContentEditable_1_1["ContentEditable"]
                });
            },
            function (ContentEditableResult_1_1) {
                exports_1({
                    "ContentEditableResult": ContentEditableResult_1_1["ContentEditableResult"]
                });
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map