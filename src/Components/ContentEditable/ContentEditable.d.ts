import { ElementRef, Renderer, EventEmitter } from "@angular/core";
import { ContentEditableResult } from "./ContentEditableResult";
export declare class ContentEditable {
    private el;
    private renderer;
    ContentEdited: EventEmitter<ContentEditableResult>;
    model: any;
    mpn: string;
    onDoubleClick(): void;
    private isEditable;
    onBlur(): void;
    onFocus(): void;
    /**
    * Input for Determining which Service to
    * to use to save the Data.
    */
    serviceName: string;
    private hasValueChanged;
    private value;
    constructor(el: ElementRef, renderer: Renderer);
    ngOnInit(): void;
    private CreateEventsForAttribute();
    private CreateContentEditableEnabled();
    /**
     * Disables editing on Element and when blur occurs
     * @example <caption>Example usage of method1.</caption>
     * // returns 2
     * globalNS.method1(5, 10);
     * @returns {Number} Returns the value of x for the equation.
     */
    private CreateContentEditableDisabled();
}
