import { Component, OnInit, forwardRef, Input, ElementRef,
    EventEmitter, Output, AfterViewInit } from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Calendar } from "tsg-calendar-lib";


export const CalendarControl_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CalendarControl),
    multi: true
};


@Component({
    selector: "sfm-calendar",
    moduleId: __moduleName,
    templateUrl: "./index.html",
    providers: [CalendarControl_VALUE_ACCESSOR]
})




export class CalendarControl implements OnInit, AfterViewInit, ControlValueAccessor {


    public Cal: Calendar;
    private innerValue: Date = new Date();
    private yearSelectorIndex = 0; 
    private hasYearChanged = false;
    
    public CalendarDate?: Date = null;
    public CalendarShown = false;
    public YearSelectorShown = false;
    public selectedDay =  -1;
    public selectedYear =  -1;

    
    public SelectableYears = new Array<number>();

    @Input() public InitialValue: string;
    @Input('previous-years') public previousYears = 20;
    @Input('future-years') public futureYears = 20;
    @Output('change') public onChange: EventEmitter<Date> = new EventEmitter<Date>();



    get value(): any {
        return this.innerValue;
    }

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.CalendarDate = v;
            this.onChangeCallback(v);
                        

         if (this.hasYearChanged && this.yearSelectorIndex !== 0) {
            this.onChange.next(v);
            this.hasYearChanged = false;
            this.yearSelectorIndex = 0;
         }
          
        }
    }

    // Set touched on blur
    public onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    public writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    public registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    public registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    

    constructor() {

        let cDate = new Date();

        let year = cDate.getFullYear();
        let month = cDate.getMonth() + 1;
        
        this.Cal = new Calendar(year, month);
        
        

    }


    private onTouchedCallback() { }
    private onChangeCallback(_: any) { }


    public SelectDate(day: number, el: ElementRef): Date {

        // console.log(el.nativeElement);

        this.selectedDay = day;

        // console.log(`this.SelectedDay: ${this.selectedDay}`);

        if (day === 0) {
            return null;
        }
        
        this.value = new Date(this.Cal.currentYear, this.Cal.currentMonth, day);
        this.innerValue = this.value;
        // console.log("Selected Date: " + this.value.toDateString());
        // console.log(`${this.Cal.currentYear}-${this.Cal.currentMonth + 1}-${day}`);
        this.CloseCalendar();

        return this.value;
    }


    public NextMonth(): void {
        let currentMonth = this.Cal.currentMonth + 1;
        let currentYear = this.Cal.currentYear;


        currentMonth++;
        // console.log("Current Month Value: " + currentMonth);

        if (currentMonth > 12) {
            currentMonth = 1;
            this.Cal.currentYear++;
            currentYear = this.Cal.currentYear;
        }

        // console.log(`Current Month ${currentMonth} \n Current Year ${currentYear}`);
        this.Cal = new Calendar(currentYear, currentMonth);
        // console.log(this.Cal);
    }

    public PreviousMonth(): void {
        let currentMonth = this.Cal.currentMonth;  // Increase by 1 for 0 based Index
        let currentYear = this.Cal.currentYear;

        currentMonth--;

        if (currentMonth < 0) {
            currentMonth = 11;
             this.Cal.currentYear--;
            currentYear = this.Cal.currentYear;
            
        }

        // // console.log(`Current Month ${currentMonth} \n Current Year ${currentYear}`);
        this.Cal = new Calendar(currentYear, currentMonth + 1);
        // // console.log(this.Cal);
    }

    public ngOnInit() {
       

        if (this.InitialValue !== undefined) {
            this.value = this.InitialValue;
            let tempDate = new Date(this.InitialValue);
            // console.log(`TempDate: ${tempDate}`);
            // console.log(`Calendar Date: Year: ${tempDate.getFullYear()}  
            // Month: ${tempDate.getMonth()} `);
            this.Cal = new Calendar(tempDate.getFullYear(), tempDate.getMonth());
            
        }

    }

    public ngAfterViewInit(): void {      

        this.populateSelectableYears(this.Cal.currentYear);
    }

   public  onCalendarClick(): void { 
        this.ShowCalendar();

    }

    public ShowCalendar(): void {
        this.CalendarShown = true;

    }

    public CloseCalendar(): void { 
        this.CalendarShown = false;
    }

    public showYearSelector(): void {
        
        this.ToggleYearSelector();
    }

    public ToggleYearSelector(): void {

        this.YearSelectorShown = (this.YearSelectorShown) ? false : true;

    }

    public isSelectedDay(day: string) {

        let currentDay = parseInt(day);

    }

    public calendarYearChanged(year: string): void {

        
        // Set the Year to newly Selected Year
        this.Cal.currentYear = parseInt(year);
       
        this.ToggleYearSelector();

        this.setActiveDate(this.Cal);

        this.hasYearChanged = true;
        


    }

    public setActiveDate(cal: Calendar): void {      

        this.Cal = new Calendar(cal.currentYear, (cal.currentMonth + 1));
    }

    private populateSelectableYears(year: number): void {        
        
        for (let i = 0; i < this.previousYears; i++) {
            
            this.SelectableYears.push(year - i);
            
        }

        for (let i = 0; i < this.futureYears; i++) {
            this.SelectableYears.push(year + i);            
            
        }

        let dup = this.SelectableYears.findIndex((x) => {
           return  x = year;
        });

        if (dup !== -1) {
            this.SelectableYears.splice(dup, 1);            
        }

        this.SelectableYears.sort((x, y) => {
            return x - y;
        });

    
        
    }


}
