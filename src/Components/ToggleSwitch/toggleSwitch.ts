import { Component, OnInit, ViewChild, forwardRef, Input
 } from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';


export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ToggleSwitch),
    multi: true
};

declare const __moduleName: string;

@Component({
    moduleId: __moduleName,
    selector: "sfm-toggle",
    templateUrl: "./toggleSwitch.html",
    providers: [ Location, CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR ]
})




export class ToggleSwitch implements OnInit, ControlValueAccessor  {

    @ViewChild('toggleContainer') tc;
    @ViewChild('toggle') tg;

    @Input("on-text") public OnText = "true";
    @Input("off-text") public OffText = "false";


    public Indicator: boolean = false;
    public onTouchedCallback: () => { };
    public onChangeCallback: (_: any) => { };

    private innerValue: boolean = false;
    get value(): any {
        return this.innerValue;
    }

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    // Set touched on blur
    public onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    public writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    public registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    public registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    public Toggle(): void {
        this.Indicator = (this.Indicator === false ? true : false);

        if (this.Indicator) {
            console.log(this.tg);
            this.tg.nativeElement.style.left = "15px";
            this.tg.nativeElement.style.background = "#919191";
            this.tc.nativeElement.style.background = "#C8C8C8";

         } else {
            this.tg.nativeElement.style.left = "";
            this.tg.nativeElement.style.background = "";
            this.tc.nativeElement.style.background = "";
        }

        this.value = this.Indicator;
    }
    constructor() { }

    public ngOnInit() {
        this.value = this.Indicator;
    }

    
}
