import { OnInit } from "@angular/core";
import { ControlValueAccessor } from '@angular/forms';
export declare const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any;
export declare class ToggleSwitch implements OnInit, ControlValueAccessor {
    tc: any;
    tg: any;
    OnText: string;
    OffText: string;
    Indicator: boolean;
    onTouchedCallback: () => {};
    onChangeCallback: (_: any) => {};
    private innerValue;
    value: any;
    onBlur(): void;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    Toggle(): void;
    constructor();
    ngOnInit(): void;
}
