import { OnInit } from "@angular/core";
import { MessageToasterService } from "./Services/index";
export declare class MessageToaster implements OnInit {
    mts: MessageToasterService;
    constructor(mts: MessageToasterService);
    ngOnInit(): void;
}
