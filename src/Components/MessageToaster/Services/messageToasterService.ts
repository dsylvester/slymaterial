import { Injectable, EventEmitter, Output, ApplicationRef } from '@angular/core';
import { Notification } from "../Notification/index";


@Injectable()
export class MessageToasterService {
    @Output() public List: EventEmitter<Array<Notification>>
        = new EventEmitter<Array<Notification>>();

    public notificationList: Array<Notification> = new Array<Notification>();
    private _showMessageContainer: boolean = true;
    private id = 0;

    get ShowMessageContainer(): boolean {
        return this._showMessageContainer;
    }

    constructor(private appref: ApplicationRef) {
        // this.notificationList = new Array<Notification>();

    }

    public AddNotification(notification: Notification, durationInMinutes = 0): void {
        this.id++;

        notification.id = this.id;
        this.notificationList.push(notification);
        this.List.emit(this.notificationList);
        this.setRemovalTimer(notification.id, durationInMinutes);
        // console.log(`Notice: ${notification}`);
        // console.log(`List: ${JSON.stringify(this.notificationList, null, 5)}`);
    }

    public RemoveNotification(id: number) {
        console.log(`RemoveNotification: ${id}`);

        let result = this.notificationList.filter((notice) => {
            if (notice.id !== id) {
                return notice;
            }
        });

        this.notificationList = result;
        console.log(this.notificationList);
        this.List.emit(result);
        this.appref.tick();

        // for (let i = 0; i < this.notificationList.length; i++) {
        //     if (this.notificationList[i].id === id) {
        //         this.notificationList.splice(i, 1);
        //         this.List.emit(this.notificationList);
        //         this.appref.tick();
        //         console.log(`RemoveNotification: 
        //             ${JSON.stringify(this.notificationList, null, 5)}`);
        //         return;
        //     }
        // }
    }

    private setRemovalTimer(id: number, seconds = 0): void {
        // console.log(`List: ${JSON.stringify(this.notificationList, null, 6)}`);
        console.log(`Set Removal Timer: ${id}`);
        let tempDuration = (seconds === 0) ? 3000 : (seconds * 1000);
        setTimeout(() => {
            // for (let i = 0; i < this.notificationList.length; i++) {
            //     if (this.notificationList[i].id === id) {
            //         this.notificationList.splice(i, 1);
            //         return;
            //     }
            this.RemoveNotification(id);
            
        }, tempDuration);
    }


}
