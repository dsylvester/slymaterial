import { EventEmitter, ApplicationRef } from '@angular/core';
import { Notification } from "../Notification/index";
export declare class MessageToasterService {
    private appref;
    List: EventEmitter<Array<Notification>>;
    notificationList: Array<Notification>;
    private _showMessageContainer;
    private id;
    readonly ShowMessageContainer: boolean;
    constructor(appref: ApplicationRef);
    AddNotification(notification: Notification, durationInMinutes?: number): void;
    RemoveNotification(id: number): void;
    private setRemovalTimer(id, seconds?);
}
