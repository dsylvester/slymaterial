System.register(["@angular/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, MessageToasterService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            MessageToasterService = /** @class */ (function () {
                function MessageToasterService(appref) {
                    // this.notificationList = new Array<Notification>();
                    this.appref = appref;
                    this.List = new core_1.EventEmitter();
                    this.notificationList = new Array();
                    this._showMessageContainer = true;
                    this.id = 0;
                }
                Object.defineProperty(MessageToasterService.prototype, "ShowMessageContainer", {
                    get: function () {
                        return this._showMessageContainer;
                    },
                    enumerable: true,
                    configurable: true
                });
                MessageToasterService.prototype.AddNotification = function (notification, durationInMinutes) {
                    if (durationInMinutes === void 0) { durationInMinutes = 0; }
                    this.id++;
                    notification.id = this.id;
                    this.notificationList.push(notification);
                    this.List.emit(this.notificationList);
                    this.setRemovalTimer(notification.id, durationInMinutes);
                    // console.log(`Notice: ${notification}`);
                    // console.log(`List: ${JSON.stringify(this.notificationList, null, 5)}`);
                };
                MessageToasterService.prototype.RemoveNotification = function (id) {
                    console.log("RemoveNotification: " + id);
                    var result = this.notificationList.filter(function (notice) {
                        if (notice.id !== id) {
                            return notice;
                        }
                    });
                    this.notificationList = result;
                    console.log(this.notificationList);
                    this.List.emit(result);
                    this.appref.tick();
                    // for (let i = 0; i < this.notificationList.length; i++) {
                    //     if (this.notificationList[i].id === id) {
                    //         this.notificationList.splice(i, 1);
                    //         this.List.emit(this.notificationList);
                    //         this.appref.tick();
                    //         console.log(`RemoveNotification: 
                    //             ${JSON.stringify(this.notificationList, null, 5)}`);
                    //         return;
                    //     }
                    // }
                };
                MessageToasterService.prototype.setRemovalTimer = function (id, seconds) {
                    var _this = this;
                    if (seconds === void 0) { seconds = 0; }
                    // console.log(`List: ${JSON.stringify(this.notificationList, null, 6)}`);
                    console.log("Set Removal Timer: " + id);
                    var tempDuration = (seconds === 0) ? 3000 : (seconds * 1000);
                    setTimeout(function () {
                        // for (let i = 0; i < this.notificationList.length; i++) {
                        //     if (this.notificationList[i].id === id) {
                        //         this.notificationList.splice(i, 1);
                        //         return;
                        //     }
                        _this.RemoveNotification(id);
                    }, tempDuration);
                };
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", core_1.EventEmitter)
                ], MessageToasterService.prototype, "List", void 0);
                MessageToasterService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [core_1.ApplicationRef])
                ], MessageToasterService);
                return MessageToasterService;
            }());
            exports_1("MessageToasterService", MessageToasterService);
        }
    };
});
//# sourceMappingURL=MessageToasterService.js.map