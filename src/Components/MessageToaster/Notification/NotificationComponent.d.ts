import { OnInit, ApplicationRef } from "@angular/core";
import { NotificationType } from "./NotificationType";
import { MessageToasterService } from "../Services/index";
export declare class NotificationComponent implements OnInit {
    messageToasterService: MessageToasterService;
    private appref;
    noticeType: NotificationType;
    list: Array<Notification>;
    constructor(messageToasterService: MessageToasterService, appref: ApplicationRef);
    ngOnInit(): void;
}
