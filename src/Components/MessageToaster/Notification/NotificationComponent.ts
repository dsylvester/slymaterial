import { Component, OnInit, Input, ApplicationRef } from "@angular/core";
import { Location } from "@angular/common";
// import { Notification } from "./Notification";
import { NotificationType } from "./NotificationType";
import { MessageToasterService } from "../Services/index";


@Component({
    selector: "sfm-notification",
    moduleId: __moduleName,
    styleUrls: [
        "./notification.css"
    ],
    templateUrl: "./notification.html",
    providers: [Location]
})


export class NotificationComponent implements OnInit {
    //  @Input() list: Array<Notification>;

    public noticeType: NotificationType;
    public list: Array<Notification> = new Array<Notification>();

    constructor(
        public messageToasterService: MessageToasterService,
        private appref: ApplicationRef
    ) {
         

    }

    public ngOnInit() {

        this.messageToasterService.List.subscribe((data) => {

            this.list = data;
            this.appref.tick();
        });
    }


}
