import { NotificationType } from "./NotificationType";

export class Notification {

    private static id = 1;

    public NoticeCssClass = "";

    constructor(public Message: string, public NoticeType: NotificationType,
                public Details: string, public NoticeIcon = "flaticon-sign",
                public id: number|null = null) {

                    this.setCssClass();
                    console.log('Notice CSS Class: : ' + this.NoticeCssClass);

                    if (id === null) {
                        id = Notification.id;                                                
                    }

                    Notification.id++;
                    

                    console.log(`id: ${id}`);
                    console.log(`Notification ID: ${Notification.id}`);

                    this.setDefaultIconForError();

    }

    private setCssClass(): void {

        switch (this.NoticeType) {
            case NotificationType.SUCCESS:
                this.NoticeCssClass = "toasterSuccessMessage";
                break;

            case NotificationType.ERROR:
                this.NoticeCssClass = "toasterErrorMessage";
                break;

            case NotificationType.WARNING:
                this.NoticeCssClass = "toasterWarningMessage";
                break;

            case NotificationType.INFO:
                this.NoticeCssClass = "toasterInfoMessage";
                break;

        }
    }

    private setDefaultIconForError(): void {

        if (this.NoticeType === NotificationType.ERROR && this.NoticeIcon === "flaticon-sign") {
            this.NoticeIcon = "fa fa-exclamation-triangle fa-2x";            
        }

    }


}
