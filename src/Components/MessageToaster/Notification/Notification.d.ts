import { NotificationType } from "./NotificationType";
export declare class Notification {
    Message: string;
    NoticeType: NotificationType;
    Details: string;
    NoticeIcon: string;
    id: number | null;
    private static id;
    NoticeCssClass: string;
    constructor(Message: string, NoticeType: NotificationType, Details: string, NoticeIcon?: string, id?: number | null);
    private setCssClass();
    private setDefaultIconForError();
}
