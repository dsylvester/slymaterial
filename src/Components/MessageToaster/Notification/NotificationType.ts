export enum NotificationType {

    INFO = 100,
    SUCCESS = 200,
    WARNING = 300,
    ERROR = 500
}
