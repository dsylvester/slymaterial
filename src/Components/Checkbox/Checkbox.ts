import { Component, OnInit, Input, 
    forwardRef, Output,
    EventEmitter} from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

export const CheckhoxControl_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => Checkbox),
    multi: true
};

@Component({
    selector: "sfm-checkbox",
    moduleId: __moduleName,
    templateUrl: "./checkbox.html",
    styleUrls: [
        "./checkbox.css"
    ]
})


export class Checkbox implements OnInit, ControlValueAccessor {

    public checked = false;
    public checkBoxState: "notChecked"|"checked" = "notChecked";
    

    private innerValue: any = false;

    @Input()    public checkedValue: any = false;     
    @Input()    public disabled = false;
    @Output()   public change: EventEmitter<boolean> = new EventEmitter<boolean>();
    
    constructor() {


    }

    private onTouchedCallback() {         
    }

    private onChangeCallback(_: boolean) { 

        this.change.emit(_);
    }   

    // From ControlValueAccessor interface
    public writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;

            this.onChangeCallback(value);
        }
    }

    // From ControlValueAccessor interface
    public registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    public registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }


    public ngOnInit(): void {
        
    }

    public ngAfterViewInit(): void {

        setTimeout(() => {
            this.writeValue(
                (this.checkedValue === undefined) ? false : this.checkedValue);
        }, 1000);
    }

    public onCheckboxClick(): void {
        this.checked = (this.checked) ? false : true;
        this.writeValue(this.checked);

        this.toggleCheckboxState();

        

    }

    private toggleCheckboxState(): void {
        this.checkBoxState = (this.checkBoxState === "checked") ? "notChecked" : "checked";
    }

    public setDisabledState(isDisabled: boolean): void { }


    get value(): boolean {
        return this.innerValue;
    }
}
