import { OnInit, EventEmitter } from "@angular/core";
import { ControlValueAccessor } from "@angular/forms";
export declare const CheckhoxControl_VALUE_ACCESSOR: any;
export declare class Checkbox implements OnInit, ControlValueAccessor {
    checked: boolean;
    checkBoxState: "notChecked" | "checked";
    private innerValue;
    checkedValue: any;
    disabled: boolean;
    change: EventEmitter<boolean>;
    constructor();
    private onTouchedCallback();
    private onChangeCallback(_);
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    ngOnInit(): void;
    ngAfterViewInit(): void;
    onCheckboxClick(): void;
    private toggleCheckboxState();
    setDisabledState(isDisabled: boolean): void;
    readonly value: boolean;
}
