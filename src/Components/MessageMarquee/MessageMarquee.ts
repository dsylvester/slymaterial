import { Component, OnInit, trigger, style, state, transition, animate } from "@angular/core";
import { MessageMarqueeService } from "./Services/MessageMarqueeService";
import { MarqueeNotice, MarqueeStatus } from "../../index";

@Component({
    selector: "sfm-marquee",
    moduleId: __moduleName,
    templateUrl: "./messageMarquee.html",
    providers: [Location],
    animations: [
        trigger('marqueeOpening', [
            state('1', style({
                'height': '38px',
                'line-height': '38px',
                'width': '100%',
                'opacity': 1
            })),
            state('2', style({
                'height': '0px',
                'line-height': '0px',
                'width': '0%',
                'opacity': .1
            })),
            transition('1 => 2', animate('500ms ease-in')),
            transition('2 => 1', animate('100ms ease-out'))
        ])
    ]
})


export class MessageMarquee implements OnInit {

    public notice: MarqueeNotice;


    constructor(private mms: MessageMarqueeService) {

        this.notice = new MarqueeNotice("", MarqueeStatus.Close);



    }

    public ngOnInit() {
        this.mms.showMarquee.subscribe((x) => {
            // console.log("x: " + JSON.stringify(x, null, 5));
            this.notice = x;
        });
        // console.log(this.notice);
    }

    public OpenMe(): void {
        this.mms.Open(" I am Legend!!!!!!");
        setTimeout(() => {
            alert("Closing Message Marquee");
            this.mms.Close();
        }, 5000);
    }

    public AutoClose(): void {
         this.mms.Close();
    }

}
