System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var MarqueeStatus;
    return {
        setters: [],
        execute: function () {
            (function (MarqueeStatus) {
                MarqueeStatus[MarqueeStatus["Open"] = 1] = "Open";
                MarqueeStatus[MarqueeStatus["Close"] = 2] = "Close";
            })(MarqueeStatus || (MarqueeStatus = {}));
            exports_1("MarqueeStatus", MarqueeStatus);
        }
    };
});
//# sourceMappingURL=MarqueeStatus.js.map