System.register(["@angular/core", "../../../index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, index_1, MessageMarqueeService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }
        ],
        execute: function () {
            MessageMarqueeService = /** @class */ (function () {
                function MessageMarqueeService() {
                    this.showMarquee = new core_1.EventEmitter();
                    this.showMarquee.emit(new index_1.MarqueeNotice("", index_1.MarqueeStatus.Close));
                }
                MessageMarqueeService.prototype.Close = function (duration) {
                    var _this = this;
                    if (duration === void 0) { duration = 5000; }
                    setTimeout(function () {
                        _this.showMarquee.emit(new index_1.MarqueeNotice("", index_1.MarqueeStatus.Close));
                    }, duration);
                };
                MessageMarqueeService.prototype.Open = function (message) {
                    this.showMarquee.emit(new index_1.MarqueeNotice(message, index_1.MarqueeStatus.Open));
                };
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", core_1.EventEmitter)
                ], MessageMarqueeService.prototype, "showMarquee", void 0);
                MessageMarqueeService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [])
                ], MessageMarqueeService);
                return MessageMarqueeService;
            }());
            exports_1("MessageMarqueeService", MessageMarqueeService);
        }
    };
});
//# sourceMappingURL=MessageMarqueeService.js.map