import { Injectable, EventEmitter, Output } from '@angular/core';
import { MarqueeStatus, MarqueeNotice } from "../../../index";

@Injectable()
export class MessageMarqueeService {

    @Output() public showMarquee: EventEmitter<MarqueeNotice> = new EventEmitter<MarqueeNotice>();

    constructor() {
        this.showMarquee.emit(new MarqueeNotice("", MarqueeStatus.Close));

    }

    public Close(duration: number = 5000): void {
        setTimeout(() => {
            this.showMarquee.emit(new MarqueeNotice("", MarqueeStatus.Close));
        }, duration);

    }

    public Open(message: string) {

        this.showMarquee.emit(new MarqueeNotice(message, MarqueeStatus.Open));
    }


}
