import { EventEmitter } from '@angular/core';
import { MarqueeNotice } from "../../../index";
export declare class MessageMarqueeService {
    showMarquee: EventEmitter<MarqueeNotice>;
    constructor();
    Close(duration?: number): void;
    Open(message: string): void;
}
