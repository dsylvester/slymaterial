import { OnInit } from "@angular/core";
import { MessageMarqueeService } from "./Services/MessageMarqueeService";
import { MarqueeNotice } from "../../index";
export declare class MessageMarquee implements OnInit {
    private mms;
    notice: MarqueeNotice;
    constructor(mms: MessageMarqueeService);
    ngOnInit(): void;
    OpenMe(): void;
    AutoClose(): void;
}
