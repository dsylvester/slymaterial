import {    Component, OnInit, Input, 
    trigger, state, style, 
    transition, animate, 
    AfterViewInit } from "@angular/core";


@Component({
selector: "sfm-action-button",
moduleId: __moduleName,
templateUrl: "./Html/ActionButton.html",
styleUrls: [ "./Html/ActionButton.css"],
animations: [ ]
})

export class ActionButton implements OnInit, AfterViewInit {

    @Input('disabled') public disabled = false;


    public constructor() {

    }


    public ngOnInit(): void {

    }

    public ngAfterViewInit(): void {

    }

}
