import { OnInit, AfterViewInit, ViewContainerRef, ComponentFactoryResolver } from "@angular/core";
import { DialogService } from "./DialogService";
export declare class Dialog implements OnInit, AfterViewInit {
    dialogService: DialogService;
    cfr: ComponentFactoryResolver;
    Title: string;
    hasError: boolean;
    modalContainer: ViewContainerRef;
    ShowModal: boolean;
    constructor(dialogService: DialogService, cfr: ComponentFactoryResolver);
    ngOnInit(): void;
    ngAfterViewInit(): void;
}
