import { EventEmitter, ViewContainerRef, Type, ComponentFactoryResolver } from '@angular/core';
export declare class DialogService {
    private crf;
    /**
     * Event that tells the modal to open or close
     * via the ShowModal property on Dialog
     *
     * @event DialogService#ShowModal
     * @fires true or false to ModalStatus on Dialog
     * @property {boolean} _showModal Tells the modal to open or close
     */
    onModalWindowStatus: EventEmitter<boolean>;
    /**
     * Event that returns the data from the modal
     *
     * @event DialogService#result
     * @fires Generic result from Modal
     * @property {<T>} result
     */
    result: EventEmitter<any>;
    private _showModal;
    modalWrapper: ViewContainerRef;
    readonly ShowModal: boolean;
    constructor(crf: ComponentFactoryResolver);
    Open(comp: Type<{}>): void;
    Close(): void;
    ToggleModal(): void;
}
