import { Injectable, EventEmitter, Output,
        ViewContainerRef, Type, ComponentFactoryResolver } from '@angular/core';


@Injectable()
export class DialogService {

    /**
     * Event that tells the modal to open or close
     * via the ShowModal property on Dialog
     *
     * @event DialogService#ShowModal
     * @fires true or false to ModalStatus on Dialog
     * @property {boolean} _showModal Tells the modal to open or close
     */
    @Output() public onModalWindowStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

    /**
     * Event that returns the data from the modal
     *
     * @event DialogService#result
     * @fires Generic result from Modal
     * @property {<T>} result
     */
    @Output() public result: EventEmitter<any> = new EventEmitter<any>();


    private _showModal = true;
    public modalWrapper: ViewContainerRef;

    get ShowModal(): boolean {
        return this._showModal;
    }


    constructor(private crf: ComponentFactoryResolver) {
       console.log('Inside Dialog Service');
       console.log(`Show Modal: ${this._showModal}`);
       this.onModalWindowStatus.emit(this._showModal);
    }

    public Open(comp: Type<{}>): void {
        if (comp === undefined || comp === null) {
            throw new Error("You must provide a Component to Load into the Modal!");
        }

        this._showModal = true;
        this.onModalWindowStatus.emit(this._showModal);

        console.log(this.modalWrapper);
        this.modalWrapper.createComponent(this.crf.resolveComponentFactory(comp));
    }

    public Close(): void {
        this._showModal = false;
        this.onModalWindowStatus.emit(this._showModal);
    }

    public ToggleModal(): void {
        this._showModal = (this._showModal === false) ? true : false;
        this.onModalWindowStatus.emit(this._showModal);
    }

}
