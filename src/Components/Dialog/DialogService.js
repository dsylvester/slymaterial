System.register(["@angular/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, DialogService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            DialogService = /** @class */ (function () {
                function DialogService(crf) {
                    this.crf = crf;
                    /**
                     * Event that tells the modal to open or close
                     * via the ShowModal property on Dialog
                     *
                     * @event DialogService#ShowModal
                     * @fires true or false to ModalStatus on Dialog
                     * @property {boolean} _showModal Tells the modal to open or close
                     */
                    this.onModalWindowStatus = new core_1.EventEmitter();
                    /**
                     * Event that returns the data from the modal
                     *
                     * @event DialogService#result
                     * @fires Generic result from Modal
                     * @property {<T>} result
                     */
                    this.result = new core_1.EventEmitter();
                    this._showModal = true;
                    console.log('Inside Dialog Service');
                    console.log("Show Modal: " + this._showModal);
                    this.onModalWindowStatus.emit(this._showModal);
                }
                Object.defineProperty(DialogService.prototype, "ShowModal", {
                    get: function () {
                        return this._showModal;
                    },
                    enumerable: true,
                    configurable: true
                });
                DialogService.prototype.Open = function (comp) {
                    if (comp === undefined || comp === null) {
                        throw new Error("You must provide a Component to Load into the Modal!");
                    }
                    this._showModal = true;
                    this.onModalWindowStatus.emit(this._showModal);
                    console.log(this.modalWrapper);
                    this.modalWrapper.createComponent(this.crf.resolveComponentFactory(comp));
                };
                DialogService.prototype.Close = function () {
                    this._showModal = false;
                    this.onModalWindowStatus.emit(this._showModal);
                };
                DialogService.prototype.ToggleModal = function () {
                    this._showModal = (this._showModal === false) ? true : false;
                    this.onModalWindowStatus.emit(this._showModal);
                };
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", core_1.EventEmitter)
                ], DialogService.prototype, "onModalWindowStatus", void 0);
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", core_1.EventEmitter)
                ], DialogService.prototype, "result", void 0);
                DialogService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [core_1.ComponentFactoryResolver])
                ], DialogService);
                return DialogService;
            }());
            exports_1("DialogService", DialogService);
        }
    };
});
//# sourceMappingURL=DialogService.js.map