import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
    selector: "[dialog-template]"
})

export class DialogTemplate {
    constructor(public vcref: ViewContainerRef) {

    }
}
