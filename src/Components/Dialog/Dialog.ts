import {
    Component, OnInit, AfterViewInit, ViewChild,
    ViewContainerRef, ComponentFactoryResolver,
    Input
} from "@angular/core";
import { DialogService } from "./DialogService";



@Component({
    selector: "sfm-dialog",
    moduleId: __moduleName,
    templateUrl: "./Dialog.html",
    styleUrls: ["./Dialog.css"],
    host: {
        class: "dialogContainer"

    },
    providers: [Location],
    animations: [


    ]
})



export class Dialog implements OnInit, AfterViewInit {


    public Title: string;
    public hasError = false;


    @ViewChild("modalWrapper", { read: ViewContainerRef }) public modalContainer: ViewContainerRef;
    @Input() public ShowModal = false;

    constructor(public dialogService: DialogService, public cfr: ComponentFactoryResolver) {

        console.log("Dialog Loaded!");
        this.dialogService.ToggleModal();
        console.log("ShowModal: " + this.dialogService.ShowModal);


    }


    public ngOnInit() {

    }

    public ngAfterViewInit() {
        console.log(this.modalContainer);
        this.dialogService.modalWrapper = this.modalContainer;

    }




}
