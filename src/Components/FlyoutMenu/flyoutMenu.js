System.register(["@angular/core", "@angular/router", "./FlyoutMenuService", "./FlyOutMenuEvents"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, FlyoutMenuService_1, FlyOutMenuEvents_1, FlyoutMenu;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (FlyoutMenuService_1_1) {
                FlyoutMenuService_1 = FlyoutMenuService_1_1;
            },
            function (FlyOutMenuEvents_1_1) {
                FlyOutMenuEvents_1 = FlyOutMenuEvents_1_1;
            }
        ],
        execute: function () {
            FlyoutMenu = /** @class */ (function () {
                function FlyoutMenu(fms, router) {
                    this.fms = fms;
                    this.router = router;
                }
                FlyoutMenu.prototype.determineEvents = function (evt) {
                    switch (evt.EventType) {
                        case FlyOutMenuEvents_1.FlyOutMenuEvents.OPEN_MENU:
                            this.onMenuOpen(evt);
                            break;
                        case FlyOutMenuEvents_1.FlyOutMenuEvents.CLOSE_MENU:
                            this.onMenuClose(evt);
                            break;
                    }
                };
                FlyoutMenu.prototype.ngOnInit = function () {
                    var _this = this;
                    this.fms.onMenuEvent$.subscribe(function (evt) {
                        _this.determineEvents(evt);
                    });
                };
                FlyoutMenu.prototype.ngAfterViewInit = function () {
                    console.log(this.menuItems);
                };
                FlyoutMenu.prototype.onMenuClose = function (evt) {
                };
                FlyoutMenu.prototype.onMenuOpen = function (evt) {
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Array)
                ], FlyoutMenu.prototype, "menuItems", void 0);
                FlyoutMenu = __decorate([
                    core_1.Component({
                        selector: "sfm-flyout-menu",
                        moduleId: __moduleName,
                        templateUrl: "./FlyoutMenu.html",
                        host: {
                            class: "flyoutMenuWrapper"
                        },
                        animations: []
                    }),
                    __metadata("design:paramtypes", [FlyoutMenuService_1.FlyoutMenuService, router_1.Router])
                ], FlyoutMenu);
                return FlyoutMenu;
            }());
            exports_1("FlyoutMenu", FlyoutMenu);
        }
    };
});
//# sourceMappingURL=FlyoutMenu.js.map