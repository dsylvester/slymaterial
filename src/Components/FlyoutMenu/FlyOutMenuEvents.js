System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var FlyOutMenuEvents;
    return {
        setters: [],
        execute: function () {
            (function (FlyOutMenuEvents) {
                FlyOutMenuEvents[FlyOutMenuEvents["CLOSE_MENU"] = 100] = "CLOSE_MENU";
                FlyOutMenuEvents[FlyOutMenuEvents["OPEN_MENU"] = 200] = "OPEN_MENU";
            })(FlyOutMenuEvents || (FlyOutMenuEvents = {}));
            exports_1("FlyOutMenuEvents", FlyOutMenuEvents);
        }
    };
});
//# sourceMappingURL=FlyOutMenuEvents.js.map