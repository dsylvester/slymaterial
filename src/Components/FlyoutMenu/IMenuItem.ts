export interface IMenuItem {

    Name: string;
    Url: string;
    Icon?: string;
    Parent: boolean;

}
