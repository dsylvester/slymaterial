import Rx from "rxjs/Rx";
import { IFlyOutMenuEvent } from "./IFlyOutMenuEvent";
import { WindowRef } from "../../Services/index";
export declare class FlyoutMenuService {
    private windowRef;
    onMenuEvent$: Rx.Subject<IFlyOutMenuEvent>;
    private window;
    menuShown: boolean;
    constructor(windowRef: WindowRef);
    OpenMenu(): void;
    CloseMenu(): void;
    ToggleMenu(): void;
    NavigateTo(url: any): void;
}
