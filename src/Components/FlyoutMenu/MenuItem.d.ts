import { IMenuItem } from "./IMenuItem";
export declare class MenuItem implements IMenuItem {
    Name: string;
    Url: string;
    Icon: string;
    Parent: boolean;
    constructor(Name: string, Url: string, Icon?: string, Parent?: boolean);
}
