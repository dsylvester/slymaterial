import { OnInit, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { FlyoutMenuService } from "./FlyoutMenuService";
import { IFlyOutMenuEvent } from "./IFlyOutMenuEvent";
export declare class FlyoutMenu implements OnInit, AfterViewInit {
    private fms;
    private router;
    private menuItems;
    constructor(fms: FlyoutMenuService, router: Router);
    private determineEvents(evt);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    onMenuClose(evt: IFlyOutMenuEvent): void;
    onMenuOpen(evt: IFlyOutMenuEvent): void;
}
