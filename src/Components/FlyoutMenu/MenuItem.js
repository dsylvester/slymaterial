System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var MenuItem;
    return {
        setters: [],
        execute: function () {
            MenuItem = /** @class */ (function () {
                function MenuItem(Name, Url, Icon, Parent) {
                    if (Parent === void 0) { Parent = true; }
                    this.Name = Name;
                    this.Url = Url;
                    this.Icon = Icon;
                    this.Parent = Parent;
                }
                return MenuItem;
            }());
            exports_1("MenuItem", MenuItem);
        }
    };
});
//# sourceMappingURL=MenuItem.js.map