import { IMenuItem } from "./IMenuItem";

export class MenuItem implements IMenuItem {

    constructor(
        public Name: string,
        public Url: string, 
        public Icon?: string,
        public Parent: boolean = true
    ) {

    }
}
