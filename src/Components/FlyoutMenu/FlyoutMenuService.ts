import { Injectable, Output } from '@angular/core';
import Rx from "rxjs/Rx";
import { IFlyOutMenuEvent } from "./IFlyOutMenuEvent";
import { FlyOutMenuEvents } from "./FlyOutMenuEvents";
import { WindowRef } from "../../Services/index";


@Injectable()
export class FlyoutMenuService {

    @Output() public onMenuEvent$: Rx.Subject<IFlyOutMenuEvent> =
         new Rx.Subject<IFlyOutMenuEvent>();
    
    private window: Window;

    public menuShown = true;

    constructor(private windowRef: WindowRef) {
        
       this.window = this.windowRef.GetNativeWindow();

    }
   
    public OpenMenu(): void {
        this.menuShown = true;
        this.onMenuEvent$.next({ 
            EventType: FlyOutMenuEvents.OPEN_MENU, 
            EventData: null
        });
    }
    
    public CloseMenu(): void {
        this.menuShown = false;
        this.onMenuEvent$.next({ EventType: FlyOutMenuEvents.CLOSE_MENU, EventData: null});

    }

    public ToggleMenu(): void {

        this.menuShown = (this.menuShown) ? false : true;

    }

    public NavigateTo(url): void {
        this.ToggleMenu();
        
        this.window
            .open(url);
    }

   
}
