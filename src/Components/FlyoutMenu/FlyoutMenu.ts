import { Component, OnInit, Input, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import { FlyoutMenuService } from "./FlyoutMenuService";
import { IFlyOutMenuEvent } from "./IFlyOutMenuEvent";
import { MenuItem } from "./MenuItem";
import { FlyOutMenuEvents } from "./FlyOutMenuEvents";



@Component({
    selector: "sfm-flyout-menu",
    moduleId: __moduleName,
    templateUrl: "./FlyoutMenu.html",
    host: {
        class : "flyoutMenuWrapper"
        
    },
    animations: [


    ]
})



export class FlyoutMenu implements OnInit, AfterViewInit  {   
    

    @Input() private menuItems: Array<MenuItem>;

    constructor(private fms: FlyoutMenuService, private router: Router) {

        

    }

    private determineEvents(evt: IFlyOutMenuEvent): void {

        switch (evt.EventType) {

            case FlyOutMenuEvents.OPEN_MENU:
                this.onMenuOpen(evt);
                break;

            case FlyOutMenuEvents.CLOSE_MENU:
                this.onMenuClose(evt);
                break;
        }
    }

   
    public ngOnInit() {

        this.fms.onMenuEvent$.subscribe((evt) => {

            this.determineEvents(evt);
        });

    }

    public ngAfterViewInit(): void {
        
        console.log(this.menuItems);

    }

    public onMenuClose(evt: IFlyOutMenuEvent): void {

        
    }

    public onMenuOpen(evt: IFlyOutMenuEvent): void {
        
    }

}
