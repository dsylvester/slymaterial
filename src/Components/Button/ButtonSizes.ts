export enum ButtonSizes {
    SMALL = 10,
    MEDIUM = 20,
    LARGE = 30,
    EXTRA_LARGE = 40
}
