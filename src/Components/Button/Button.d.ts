import { OnInit, AfterViewInit } from "@angular/core";
export declare class Button implements OnInit, AfterViewInit {
    Disabled: boolean;
    Size: "sm" | "med" | "lg";
    buttonSize: number;
    constructor();
    ngOnInit(): void;
    ngAfterViewInit(): void;
    private determinButtonSize(size);
}
