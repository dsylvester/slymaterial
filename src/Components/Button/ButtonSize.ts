let buttonSize = new Map<string, number>();

buttonSize.set("sm", 75);
buttonSize.set("med", 150);
buttonSize.set("lg", 175);
buttonSize.set("xlg", 200);

export const ButtonSize: Map<string, number> = buttonSize;
