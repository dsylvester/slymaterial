import {    Component, OnInit, Input, 
            trigger, state, style, 
            transition, animate, 
            AfterViewInit } from "@angular/core";
import { ButtonSize } from "./ButtonSize";

@Component({
    selector: "sfm-button",
    moduleId: __moduleName,
    templateUrl: "./Button.html",
    styleUrls: [ "./Buttons.css"],
    providers: [Location],
    animations: [
        trigger('marqueeOpening', [
            state('1', style({
                'height': '38px',
                'line-height': '38px',
                'width': '100%',
                'opacity': 1
            })),
            state('2', style({
                'height': '0px',
                'line-height': '0px',
                'width': '0%',
                'opacity': .1
            })),
            transition('1 => 2', animate('500ms ease-in')),
            transition('2 => 1', animate('100ms ease-out'))
        ])
    ]
})



export class Button implements OnInit, AfterViewInit  {

    @Input() public Disabled: boolean;
    @Input() public Size: "sm" | "med" | "lg";

    public buttonSize = 75;

    constructor() {

}

    public ngOnInit() {



}

    public ngAfterViewInit(): void {

        setTimeout(() => {
            this.determinButtonSize(this.Size);
            console.log(`ButtonSize: ${this.buttonSize}`);
        }, 1000);

    }

    private determinButtonSize(size: string): void {
        this.buttonSize = (ButtonSize.get(size) === undefined) ?
            ButtonSize.get("sm") : ButtonSize.get(size);
    }


}
