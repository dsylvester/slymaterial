System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ButtonSizes;
    return {
        setters: [],
        execute: function () {
            (function (ButtonSizes) {
                ButtonSizes[ButtonSizes["SMALL"] = 10] = "SMALL";
                ButtonSizes[ButtonSizes["MEDIUM"] = 20] = "MEDIUM";
                ButtonSizes[ButtonSizes["LARGE"] = 30] = "LARGE";
                ButtonSizes[ButtonSizes["EXTRA_LARGE"] = 40] = "EXTRA_LARGE";
            })(ButtonSizes || (ButtonSizes = {}));
            exports_1("ButtonSizes", ButtonSizes);
        }
    };
});
//# sourceMappingURL=ButtonSizes.js.map