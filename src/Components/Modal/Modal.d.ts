import { ViewContainerRef, AfterViewInit } from "@angular/core";
import { ModalService } from "./ModalService";
export declare class Modal implements AfterViewInit {
    modalService: ModalService;
    /**
     * modalBody is a setter property
     * It is set Automatically once the ViewChild
     * is resolved and sets the Container Property
     * in Modal Service to the resolved Value
     * and Opens the Modal Window
     * @property {ViewContainerRef} modalbody
    */
    ModalBody: ViewContainerRef;
    ModalBodyContainer: ViewContainerRef;
    private modalBody;
    private _modalBodyContainer;
    constructor(modalService: ModalService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngAfterContentInit(): void;
}
