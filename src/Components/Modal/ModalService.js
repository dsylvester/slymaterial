System.register(["@angular/core", "rxjs/Rx"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, Rx_1, ModalService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            }
        ],
        execute: function () {
            ModalService = /** @class */ (function () {
                function ModalService(componentFactoryResolver) {
                    this.componentFactoryResolver = componentFactoryResolver;
                    this.showModal = false;
                    this.modalDisplay = "none";
                    this.modalTitle = "";
                    this.mode = "Normal";
                    this.onModalClose = new Rx_1.default.Subject();
                    this.hasError = false;
                    this.ViewBag = new Map();
                }
                Object.defineProperty(ModalService.prototype, "ShowModal", {
                    // public onModalOpen$: Rx.Subject<any> = new Rx.Subject();
                    get: function () {
                        return this.showModal;
                    },
                    set: function (val) {
                        this.showModal = val;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ModalService.prototype, "Container", {
                    get: function () {
                        return this._container;
                    },
                    set: function (val) {
                        this._container = val;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ModalService.prototype, "BodyContainer", {
                    get: function () {
                        return this._bodyContainer;
                    },
                    set: function (val) {
                        this._bodyContainer = val;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ModalService.prototype, "Mode", {
                    get: function () {
                        return this.mode;
                    },
                    set: function (val) {
                        this.mode = val;
                    },
                    enumerable: true,
                    configurable: true
                });
                // private createComponent(aaa): void {
                //     console.log(`Create Component`);
                //     console.log(aaa);
                //     console.log(this.Container);
                // }
                ModalService.prototype.CloseDialog = function (evtName) {
                    if (evtName === void 0) { evtName = null; }
                    console.log("Closing Modal");
                    this.resetModal();
                    if (evtName === null) {
                        this.onModalClose.next();
                    }
                    else {
                        this.onModalClose.next(evtName);
                    }
                };
                ModalService.prototype.ToggleModal = function () {
                    this.showModal = (this.showModal === true) ? false : true;
                };
                ModalService.prototype.Open = function (component, title, data, mode) {
                    if (title === void 0) { title = ""; }
                    if (data === void 0) { data = null; }
                    if (mode === void 0) { mode = "Normal"; }
                    this.showModal = true;
                    this.modalDisplay = "block";
                    this.modalTitle = title;
                    this.dynamicComponent = component;
                    this.Add("initData", data);
                    this.mode = mode;
                    console.log(component);
                    console.log(this.ViewBag);
                    // const aaa = this.componentFactoryResolver.resolveComponentFactory(component);
                };
                ModalService.prototype.OpenModal = function (el) {
                    console.log(el);
                    var comp = this.componentFactoryResolver
                        .resolveComponentFactory(this.dynamicComponent);
                    console.log("BodyContainer: " + this.BodyContainer);
                    // console.log(this.BodyContainer!.length);
                    // if (this.BodyContainer !== undefined || this.BodyContainer !== null) {
                    //     this.BodyContainer.clear();
                    // }
                    var aaa = document.getElementsByClassName('myModal');
                    // console.log(`My Modal elements: ${JSON.stringify(aaa)}`);
                    el.remove();
                    if (aaa.length > 1) {
                        aaa[1].remove();
                    }
                    // el.clear();
                    console.log("Modal Element Length: " + el.length);
                    var insertedComponent = el.createComponent(comp);
                    var instance = insertedComponent.instance;
                    var incomingData = this.Get("initData");
                    console.log(incomingData);
                    instance.data = incomingData;
                    console.log(insertedComponent);
                };
                ModalService.prototype.Add = function (name, data) {
                    console.log("Adding to ViewBag: " + name + ": " + data);
                    this.ViewBag.set(name, data);
                    console.log(this.ViewBag);
                };
                ModalService.prototype.Get = function (name) {
                    return this.ViewBag.get(name);
                };
                ModalService.prototype.resetModal = function () {
                    console.log("Closing Modal");
                    this.modalDisplay = "none";
                    this.ShowModal = false;
                    // this.mode = "Normal";
                    // this.ViewBag = new Map<string, any>();
                    console.log("this.showModal: " + this.ShowModal);
                    console.log(this.ViewBag);
                };
                ModalService = __decorate([
                    core_1.Injectable()
                    /**
                     * Provide access to the className
                     * Service web API
                     *
                     */
                    ,
                    __metadata("design:paramtypes", [core_1.ComponentFactoryResolver])
                ], ModalService);
                return ModalService;
            }());
            exports_1("ModalService", ModalService);
        }
    };
});
//# sourceMappingURL=ModalService.js.map