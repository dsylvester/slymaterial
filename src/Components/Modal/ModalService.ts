import { Injectable, Type, ComponentFactoryResolver, ViewContainerRef } from "@angular/core";
import Rx from "rxjs/Rx";
import { ModalMode } from "./CustomTypes";



@Injectable()
/**
 * Provide access to the className
 * Service web API
 *
 */
export class ModalService {

    private showModal = false;
    public modalDisplay: "none" | "block" = "none";
    public modalTitle = "";

    public ViewBag: Map<string, any>;

    private _container: ViewContainerRef;
    private _bodyContainer: ViewContainerRef;

    private dynamicComponent: Type<any>;
    private mode: ModalMode = "Normal";


    public onModalClose: Rx.Subject<any> = new Rx.Subject<any>();


    // public onModalOpen$: Rx.Subject<any> = new Rx.Subject();

    get ShowModal(): boolean {
        return this.showModal;
    }

    set ShowModal(val: boolean) {
        this.showModal = val;

    }

    set Container(val: ViewContainerRef) {
        this._container = val;
    

    }

    get Container(): ViewContainerRef {
        return this._container;

    }

    set BodyContainer(val: ViewContainerRef) {
        this._bodyContainer = val;

    }

    get BodyContainer(): ViewContainerRef {
        return this._bodyContainer;

    }

    set Mode(val: ModalMode) {
        this.mode = val;

    }

    get Mode(): ModalMode {
        return this.mode;

    }

    public hasError = false;


    constructor(private componentFactoryResolver: ComponentFactoryResolver) {

        this.ViewBag = new Map<string, any>();


    }

    // private createComponent(aaa): void {
    //     console.log(`Create Component`);
    //     console.log(aaa);
    //     console.log(this.Container);
    // }

    public CloseDialog(evtName: string | null = null): void {

        console.log("Closing Modal");
        
        this.resetModal();

        if (evtName === null) {
            this.onModalClose.next();
        } else {
            this.onModalClose.next(evtName);
        }
    }

    public ToggleModal(): void {
        this.showModal = (this.showModal === true) ? false : true;

    }

    public Open<T>(component: Type<T>, title: string = "", data: any | null = null, mode: ModalMode = "Normal"): void {
        this.showModal = true;
        this.modalDisplay = "block";
        this.modalTitle = title;
        this.dynamicComponent = component;
        this.Add("initData", data);

        this.mode = mode;

        console.log(component);
        console.log(this.ViewBag);
        // const aaa = this.componentFactoryResolver.resolveComponentFactory(component);


    }

    public OpenModal(el: ViewContainerRef): void {
        console.log(el);


        const comp =
            this.componentFactoryResolver
                .resolveComponentFactory(this.dynamicComponent);

        console.log(`BodyContainer: ${this.BodyContainer}`);
        // console.log(this.BodyContainer!.length);

        // if (this.BodyContainer !== undefined || this.BodyContainer !== null) {
        //     this.BodyContainer.clear();
        // }

        let aaa = document.getElementsByClassName('myModal');
        // console.log(`My Modal elements: ${JSON.stringify(aaa)}`);

        el.remove();

        if (aaa!.length > 1) {

            aaa[1].remove();
        }


        // el.clear();

        console.log(`Modal Element Length: ${el.length}`);

        let insertedComponent = el.createComponent(comp);

        let instance = insertedComponent.instance;

        let incomingData = this.Get("initData");

        console.log(incomingData);


        instance.data = incomingData;



        console.log(insertedComponent);



    }

    public Add(name: string, data: any): void {

        console.log(`Adding to ViewBag: ${name}: ${data}`);

        this.ViewBag.set(name, data);
        console.log(this.ViewBag);
    }

    public Get<T>(name: string): T {

        return this.ViewBag.get(name);

    }

    private resetModal(): void {

        console.log(`Closing Modal`);
        this.modalDisplay = "none";
        this.ShowModal = false;
        // this.mode = "Normal";
        // this.ViewBag = new Map<string, any>();


        console.log(`this.showModal: ${this.ShowModal}`);
        console.log(this.ViewBag);
    }
}
