import {
    Component, ViewChild, ViewContainerRef,
    AfterViewInit
} from "@angular/core";

import { ModalService } from "./ModalService";

@Component({
    selector: "sfm-modal",
    moduleId: __moduleName,
    templateUrl: "./Modal.html",
    styleUrls: ["./Modal.css"]
})



export class Modal implements AfterViewInit {

    /**
     * modalBody is a setter property
     * It is set Automatically once the ViewChild
     * is resolved and sets the Container Property
     * in Modal Service to the resolved Value
     * and Opens the Modal Window
     * @property {ViewContainerRef} modalbody     
    */
    @ViewChild('modalbody', { read: ViewContainerRef })

    set ModalBody(val: ViewContainerRef) {
        this.modalBody = val;
        this.modalService.Container = val;

        if (this.modalService.ShowModal) {
            console.log(`Setting ModalBody property`);
            setTimeout(() => {
                this.modalService.BodyContainer = this.ModalBodyContainer;
                console.log(this.ModalBodyContainer);

                this.modalService.OpenModal(val);
                console.log(`ModalBody property Set and OpenModal Called`);
                
            }, 1000);
            
        }
    }

    @ViewChild('modalbodyContainer', { read: ViewContainerRef })
    
        set ModalBodyContainer(val: ViewContainerRef) {
            this._modalBodyContainer = val;
            
    
            
            if (this.modalService.ShowModal) {
                console.log(`Setting ModalBodyContainer property`);
                this.modalService.BodyContainer = val;
                
            }
        }




    private modalBody: ViewContainerRef;
    private _modalBodyContainer: ViewContainerRef;


    constructor(public modalService: ModalService) {

        console.log("Modal Loaded!");
        // this.modalService.ToggleModal();
        console.log("ShowModal: " + this.modalService.ShowModal);


    }

    public ngOnInit(): void {


    }

    public ngAfterViewInit(): void {
        console.log(this.modalBody);
    }

    public ngAfterContentInit(): void {
        console.log(this.modalBody);
    }


}
