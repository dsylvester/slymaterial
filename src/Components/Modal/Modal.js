System.register(["@angular/core", "./ModalService"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ModalService_1, Modal;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ModalService_1_1) {
                ModalService_1 = ModalService_1_1;
            }
        ],
        execute: function () {
            Modal = /** @class */ (function () {
                function Modal(modalService) {
                    this.modalService = modalService;
                    console.log("Modal Loaded!");
                    // this.modalService.ToggleModal();
                    console.log("ShowModal: " + this.modalService.ShowModal);
                }
                Object.defineProperty(Modal.prototype, "ModalBody", {
                    /**
                     * modalBody is a setter property
                     * It is set Automatically once the ViewChild
                     * is resolved and sets the Container Property
                     * in Modal Service to the resolved Value
                     * and Opens the Modal Window
                     * @property {ViewContainerRef} modalbody
                    */
                    set: function (val) {
                        var _this = this;
                        this.modalBody = val;
                        this.modalService.Container = val;
                        if (this.modalService.ShowModal) {
                            console.log("Setting ModalBody property");
                            setTimeout(function () {
                                _this.modalService.BodyContainer = _this.ModalBodyContainer;
                                console.log(_this.ModalBodyContainer);
                                _this.modalService.OpenModal(val);
                                console.log("ModalBody property Set and OpenModal Called");
                            }, 1000);
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Modal.prototype, "ModalBodyContainer", {
                    set: function (val) {
                        this._modalBodyContainer = val;
                        if (this.modalService.ShowModal) {
                            console.log("Setting ModalBodyContainer property");
                            this.modalService.BodyContainer = val;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Modal.prototype.ngOnInit = function () {
                };
                Modal.prototype.ngAfterViewInit = function () {
                    console.log(this.modalBody);
                };
                Modal.prototype.ngAfterContentInit = function () {
                    console.log(this.modalBody);
                };
                __decorate([
                    core_1.ViewChild('modalbody', { read: core_1.ViewContainerRef }),
                    __metadata("design:type", core_1.ViewContainerRef),
                    __metadata("design:paramtypes", [core_1.ViewContainerRef])
                ], Modal.prototype, "ModalBody", null);
                __decorate([
                    core_1.ViewChild('modalbodyContainer', { read: core_1.ViewContainerRef }),
                    __metadata("design:type", core_1.ViewContainerRef),
                    __metadata("design:paramtypes", [core_1.ViewContainerRef])
                ], Modal.prototype, "ModalBodyContainer", null);
                Modal = __decorate([
                    core_1.Component({
                        selector: "sfm-modal",
                        moduleId: __moduleName,
                        templateUrl: "./Modal.html",
                        styleUrls: ["./Modal.css"]
                    }),
                    __metadata("design:paramtypes", [ModalService_1.ModalService])
                ], Modal);
                return Modal;
            }());
            exports_1("Modal", Modal);
        }
    };
});
//# sourceMappingURL=Modal.js.map