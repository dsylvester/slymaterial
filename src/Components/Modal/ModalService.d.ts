import { Type, ComponentFactoryResolver, ViewContainerRef } from "@angular/core";
import Rx from "rxjs/Rx";
import { ModalMode } from "./CustomTypes";
export declare class ModalService {
    private componentFactoryResolver;
    private showModal;
    modalDisplay: "none" | "block";
    modalTitle: string;
    ViewBag: Map<string, any>;
    private _container;
    private _bodyContainer;
    private dynamicComponent;
    private mode;
    onModalClose: Rx.Subject<any>;
    ShowModal: boolean;
    Container: ViewContainerRef;
    BodyContainer: ViewContainerRef;
    Mode: ModalMode;
    hasError: boolean;
    constructor(componentFactoryResolver: ComponentFactoryResolver);
    CloseDialog(evtName?: string | null): void;
    ToggleModal(): void;
    Open<T>(component: Type<T>, title?: string, data?: any | null, mode?: ModalMode): void;
    OpenModal(el: ViewContainerRef): void;
    Add(name: string, data: any): void;
    Get<T>(name: string): T;
    private resetModal();
}
