import { Injectable } from "@angular/core";
import { WindowRef } from "./WindowRef";





@Injectable()
export class DesktopNotificationService {

    private ApiKey = "AIzaSyBR7r5kGeIePPKRw5LmIg8p-ePEkrV0-1w";
    private apiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    private window: Window;
    private noticesAllowed = false;
    private permissionRequestCount = 1;

    constructor(private windowRef: WindowRef) {
        
        this.window = this.windowRef.GetNativeWindow();
        this.testForNotification();
    
    }

    private testForNotification(): void {
        
        console.log(this.window["Notification"]);
        console.log("Notification" in this.window);

        if ("Notification" in this.window) {
            this.hasPermission();
        }
        else {
            console.error("Notifications aren't supported in your browswer");
        }

    }

    private hasPermission(): void {

        this.window.Notification.requestPermission().then((result) => {
            console.log(` Has Permission: ${result}`);

            if ((result === "denied" || result === "default") && this.permissionRequestCount < 2) {
                this.permissionRequestCount++;
                this.hasPermission();
            }

            this.noticesAllowed = result;
            
        });

        

        return this.window.Notification.permission;
    }

    public CreateNotification(title: string, body: string,
        icon: string = "/Areas/Location/app/assets/imgs/SitePhoto.jpg"): void {

        if (this.noticesAllowed) {

            let notification = new Notification(title, { 
                body: body,
                icon: icon });

            
        }

    
    }

}
