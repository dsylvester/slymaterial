import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
export declare class GeoCodingService {
    private http;
    private ApiKey;
    private apiUrl;
    constructor(http: HttpClient);
    GetByAddress(address: string, city: string, state: string, zip: string): Observable<string> | null;
    private formatAddress(address, city, state, zip);
}
