import { WindowRef } from "./WindowRef";
export declare class DesktopNotificationService {
    private windowRef;
    private ApiKey;
    private apiUrl;
    private window;
    private noticesAllowed;
    private permissionRequestCount;
    constructor(windowRef: WindowRef);
    private testForNotification();
    private hasPermission();
    CreateNotification(title: string, body: string, icon?: string): void;
}
