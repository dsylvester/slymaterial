import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";



@Injectable()
export class GeoCodingService {

    private ApiKey = "AIzaSyBR7r5kGeIePPKRw5LmIg8p-ePEkrV0-1w";
    private apiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=";

    constructor(private http: HttpClient) {

    }

    public GetByAddress(address: string, city: string, state: string, zip: string):
        Observable<string> | null {

        let tempAddress = this.formatAddress(address, city, state, zip);


        return this.http.get(`${this.apiUrl}${tempAddress}&key=${this.ApiKey}`)
            .map((response: string) => {
                // console.log(response);
                return response;
            })
            .catch((err) => {
                console.log(`Error: ${JSON.stringify(err, null, 5)}`);
            });




    }

    private formatAddress(address: string, city: string, state: string, zip: string): string {
        return `${address},${city},${state},${zip}`.replace(" ", "+");
    }

}
