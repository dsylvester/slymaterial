System.register(["./DesktopNotificationService", "./GeoCodingService", "./WindowRef"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (DesktopNotificationService_1_1) {
                exports_1({
                    "DesktopNotificationService": DesktopNotificationService_1_1["DesktopNotificationService"]
                });
            },
            function (GeoCodingService_1_1) {
                exports_1({
                    "GeoCodingService": GeoCodingService_1_1["GeoCodingService"]
                });
            },
            function (WindowRef_1_1) {
                exports_1({
                    "WindowRef": WindowRef_1_1["WindowRef"]
                });
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map