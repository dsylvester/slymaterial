System.register(["@angular/core", "@angular/common", "@angular/platform-browser", "@angular/platform-browser/animations", "@angular/forms", "./Components/ToggleSwitch/toggleSwitch", "./Components/MessageToaster/Notification/NotificationComponent", "./Components/MessageToaster/Services/messageToasterService", "./Components/MessageToaster/MessageToaster", "./Components/MessageMarquee/MessageMarquee", "./Components/MessageMarquee/Services/MessageMarqueeService", "./Components/FlyoutMenu/index", "./Components/Button/Button", "./Components/CalendarControl/CalendarControl", "./Components/Dialog/Dialog", "./Components/Dialog/DialogService", "./Components/ContactChip/ContactChip", "./Components/DataGrid/DataGridService", "./Components/DataGrid/DataGrid", "./Components/DataGrid/DGHeader", "./Components/DataGrid/DGDataRow", "./Components/DataGrid/DGFooter", "./Components/Checkbox/Checkbox", "./Components/Modal/ModalService", "./Components/Button-Action/index", "./Components/ContentEditable/index", "./Services/index", "./Components/Modal/index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, common_1, platform_browser_1, animations_1, forms_1, toggleSwitch_1, NotificationComponent_1, messageToasterService_1, MessageToaster_1, MessageMarquee_1, MessageMarqueeService_1, index_1, Button_1, CalendarControl_1, Dialog_1, DialogService_1, ContactChip_1, DataGridService_1, DataGrid_1, DGHeader_1, DGDataRow_1, DGFooter_1, Checkbox_1, ModalService_1, index_2, index_3, index_4, index_5, SlyMaterialModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (animations_1_1) {
                animations_1 = animations_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (toggleSwitch_1_1) {
                toggleSwitch_1 = toggleSwitch_1_1;
            },
            function (NotificationComponent_1_1) {
                NotificationComponent_1 = NotificationComponent_1_1;
            },
            function (messageToasterService_1_1) {
                messageToasterService_1 = messageToasterService_1_1;
            },
            function (MessageToaster_1_1) {
                MessageToaster_1 = MessageToaster_1_1;
            },
            function (MessageMarquee_1_1) {
                MessageMarquee_1 = MessageMarquee_1_1;
            },
            function (MessageMarqueeService_1_1) {
                MessageMarqueeService_1 = MessageMarqueeService_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (Button_1_1) {
                Button_1 = Button_1_1;
            },
            function (CalendarControl_1_1) {
                CalendarControl_1 = CalendarControl_1_1;
            },
            function (Dialog_1_1) {
                Dialog_1 = Dialog_1_1;
            },
            function (DialogService_1_1) {
                DialogService_1 = DialogService_1_1;
            },
            function (ContactChip_1_1) {
                ContactChip_1 = ContactChip_1_1;
            },
            function (DataGridService_1_1) {
                DataGridService_1 = DataGridService_1_1;
            },
            function (DataGrid_1_1) {
                DataGrid_1 = DataGrid_1_1;
            },
            function (DGHeader_1_1) {
                DGHeader_1 = DGHeader_1_1;
            },
            function (DGDataRow_1_1) {
                DGDataRow_1 = DGDataRow_1_1;
            },
            function (DGFooter_1_1) {
                DGFooter_1 = DGFooter_1_1;
            },
            function (Checkbox_1_1) {
                Checkbox_1 = Checkbox_1_1;
            },
            function (ModalService_1_1) {
                ModalService_1 = ModalService_1_1;
            },
            function (index_2_1) {
                index_2 = index_2_1;
            },
            function (index_3_1) {
                index_3 = index_3_1;
            },
            function (index_4_1) {
                index_4 = index_4_1;
            },
            function (index_5_1) {
                index_5 = index_5_1;
            }
        ],
        execute: function () {
            SlyMaterialModule = /** @class */ (function () {
                function SlyMaterialModule() {
                }
                SlyMaterialModule_1 = SlyMaterialModule;
                SlyMaterialModule.forRoot = function () {
                    return {
                        ngModule: SlyMaterialModule_1,
                        providers: [
                            messageToasterService_1.MessageToasterService, MessageMarqueeService_1.MessageMarqueeService,
                            DialogService_1.DialogService, DataGridService_1.DataGridService, index_1.FlyoutMenuService, ModalService_1.ModalService,
                            index_4.WindowRef, index_4.DesktopNotificationService, index_4.GeoCodingService
                        ]
                    };
                };
                SlyMaterialModule = SlyMaterialModule_1 = __decorate([
                    core_1.NgModule({
                        imports: [
                            platform_browser_1.BrowserModule,
                            forms_1.FormsModule,
                            common_1.CommonModule,
                            animations_1.BrowserAnimationsModule
                        ],
                        exports: [toggleSwitch_1.ToggleSwitch, MessageToaster_1.MessageToaster, NotificationComponent_1.NotificationComponent,
                            MessageMarquee_1.MessageMarquee, index_1.FlyoutMenu, Button_1.Button, CalendarControl_1.CalendarControl, Dialog_1.Dialog,
                            ContactChip_1.ContactChip, DataGrid_1.DataGrid, DGHeader_1.DGHeader, DGDataRow_1.DGDataRow, DGFooter_1.DGFooter, Checkbox_1.Checkbox,
                            index_2.ActionButton, index_3.ContentEditable, index_5.Modal
                        ],
                        declarations: [toggleSwitch_1.ToggleSwitch, MessageToaster_1.MessageToaster, NotificationComponent_1.NotificationComponent,
                            MessageMarquee_1.MessageMarquee, index_1.FlyoutMenu, Button_1.Button, CalendarControl_1.CalendarControl, Dialog_1.Dialog,
                            ContactChip_1.ContactChip, DataGrid_1.DataGrid, DGHeader_1.DGHeader, DGDataRow_1.DGDataRow, DGFooter_1.DGFooter, Checkbox_1.Checkbox,
                            index_2.ActionButton, index_3.ContentEditable, index_5.Modal
                        ],
                        providers: []
                    })
                ], SlyMaterialModule);
                return SlyMaterialModule;
                var SlyMaterialModule_1;
            }());
            exports_1("SlyMaterialModule", SlyMaterialModule);
        }
    };
});
//# sourceMappingURL=app.module.js.map