System.register(["@angular/core", "@angular/common/http", "rxjs/add/operator/map", "rxjs/add/operator/catch"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, GeoCodingService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (_2) {
            }
        ],
        execute: function () {
            GeoCodingService = /** @class */ (function () {
                function GeoCodingService(http) {
                    this.http = http;
                    this.ApiKey = "AIzaSyBR7r5kGeIePPKRw5LmIg8p-ePEkrV0-1w";
                    this.apiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=";
                }
                GeoCodingService.prototype.GetByAddress = function (address, city, state, zip) {
                    var tempAddress = this.formatAddress(address, city, state, zip);
                    return this.http.get("" + this.apiUrl + tempAddress + "&key=" + this.ApiKey)
                        .map(function (response) {
                        // console.log(response);
                        return response;
                    })
                        .catch(function (err) {
                        console.log("Error: " + JSON.stringify(err, null, 5));
                    });
                };
                GeoCodingService.prototype.formatAddress = function (address, city, state, zip) {
                    return (address + "," + city + "," + state + "," + zip).replace(" ", "+");
                };
                GeoCodingService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [http_1.HttpClient])
                ], GeoCodingService);
                return GeoCodingService;
            }());
            exports_1("GeoCodingService", GeoCodingService);
        }
    };
});
//# sourceMappingURL=GeoCodingService.js.map