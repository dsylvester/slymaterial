System.register(["@angular/core", "./WindowRef"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, WindowRef_1, DesktopNotificationService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (WindowRef_1_1) {
                WindowRef_1 = WindowRef_1_1;
            }
        ],
        execute: function () {
            DesktopNotificationService = /** @class */ (function () {
                function DesktopNotificationService(windowRef) {
                    this.windowRef = windowRef;
                    this.ApiKey = "AIzaSyBR7r5kGeIePPKRw5LmIg8p-ePEkrV0-1w";
                    this.apiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=";
                    this.noticesAllowed = false;
                    this.permissionRequestCount = 1;
                    this.window = this.windowRef.GetNativeWindow();
                    this.testForNotification();
                }
                DesktopNotificationService.prototype.testForNotification = function () {
                    console.log(this.window["Notification"]);
                    console.log("Notification" in this.window);
                    if ("Notification" in this.window) {
                        this.hasPermission();
                    }
                    else {
                        console.error("Notifications aren't supported in your browswer");
                    }
                };
                DesktopNotificationService.prototype.hasPermission = function () {
                    var _this = this;
                    this.window.Notification.requestPermission().then(function (result) {
                        console.log(" Has Permission: " + result);
                        if ((result === "denied" || result === "default") && _this.permissionRequestCount < 2) {
                            _this.permissionRequestCount++;
                            _this.hasPermission();
                        }
                        _this.noticesAllowed = result;
                    });
                    return this.window.Notification.permission;
                };
                DesktopNotificationService.prototype.CreateNotification = function (title, body, icon) {
                    if (icon === void 0) { icon = "/Areas/Location/app/assets/imgs/SitePhoto.jpg"; }
                    if (this.noticesAllowed) {
                        var notification = new Notification(title, {
                            body: body,
                            icon: icon
                        });
                    }
                };
                DesktopNotificationService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [WindowRef_1.WindowRef])
                ], DesktopNotificationService);
                return DesktopNotificationService;
            }());
            exports_1("DesktopNotificationService", DesktopNotificationService);
        }
    };
});
//# sourceMappingURL=DesktopNotificationService.js.map