System.register(["@angular/core", "./Services/MessageMarqueeService", "../../index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, MessageMarqueeService_1, index_1, MessageMarquee;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (MessageMarqueeService_1_1) {
                MessageMarqueeService_1 = MessageMarqueeService_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }
        ],
        execute: function () {
            MessageMarquee = /** @class */ (function () {
                function MessageMarquee(mms) {
                    this.mms = mms;
                    this.notice = new index_1.MarqueeNotice("", index_1.MarqueeStatus.Close);
                }
                MessageMarquee.prototype.ngOnInit = function () {
                    var _this = this;
                    this.mms.showMarquee.subscribe(function (x) {
                        // console.log("x: " + JSON.stringify(x, null, 5));
                        _this.notice = x;
                    });
                    // console.log(this.notice);
                };
                MessageMarquee.prototype.OpenMe = function () {
                    var _this = this;
                    this.mms.Open(" I am Legend!!!!!!");
                    setTimeout(function () {
                        alert("Closing Message Marquee");
                        _this.mms.Close();
                    }, 5000);
                };
                MessageMarquee.prototype.AutoClose = function () {
                    this.mms.Close();
                };
                MessageMarquee = __decorate([
                    core_1.Component({
                        selector: "sfm-marquee",
                        moduleId: __moduleName,
                        templateUrl: "./messageMarquee.html",
                        providers: [Location],
                        animations: [
                            core_1.trigger('marqueeOpening', [
                                core_1.state('1', core_1.style({
                                    'height': '38px',
                                    'line-height': '38px',
                                    'width': '100%',
                                    'opacity': 1
                                })),
                                core_1.state('2', core_1.style({
                                    'height': '0px',
                                    'line-height': '0px',
                                    'width': '0%',
                                    'opacity': .1
                                })),
                                core_1.transition('1 => 2', core_1.animate('500ms ease-in')),
                                core_1.transition('2 => 1', core_1.animate('100ms ease-out'))
                            ])
                        ]
                    }),
                    __metadata("design:paramtypes", [MessageMarqueeService_1.MessageMarqueeService])
                ], MessageMarquee);
                return MessageMarquee;
            }());
            exports_1("MessageMarquee", MessageMarquee);
        }
    };
});
//# sourceMappingURL=MessageMarquee.js.map