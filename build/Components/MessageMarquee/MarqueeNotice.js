System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var MarqueeNotice;
    return {
        setters: [],
        execute: function () {
            MarqueeNotice = /** @class */ (function () {
                function MarqueeNotice(Message, State) {
                    this.Message = Message;
                    this.State = State;
                }
                return MarqueeNotice;
            }());
            exports_1("MarqueeNotice", MarqueeNotice);
        }
    };
});
//# sourceMappingURL=MarqueeNotice.js.map