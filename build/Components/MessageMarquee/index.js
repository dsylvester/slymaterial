System.register(["./Services/index", "./MarqueeNotice", "./MarqueeStatus", "./MessageMarquee"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var exportedNames_1 = {
        "MarqueeNotice": true,
        "MarqueeStatus": true,
        "MessageMarquee": true
    };
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default" && !exportedNames_1.hasOwnProperty(n)) exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (index_1_1) {
                exportStar_1(index_1_1);
            },
            function (MarqueeNotice_1_1) {
                exports_1({
                    "MarqueeNotice": MarqueeNotice_1_1["MarqueeNotice"]
                });
            },
            function (MarqueeStatus_1_1) {
                exports_1({
                    "MarqueeStatus": MarqueeStatus_1_1["MarqueeStatus"]
                });
            },
            function (MessageMarquee_1_1) {
                exports_1({
                    "MessageMarquee": MessageMarquee_1_1["MessageMarquee"]
                });
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map