System.register(["@angular/core", "./ContentEditableResult"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ContentEditableResult_1, ContentEditable;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ContentEditableResult_1_1) {
                ContentEditableResult_1 = ContentEditableResult_1_1;
            }
        ],
        execute: function () {
            ContentEditable = /** @class */ (function () {
                function ContentEditable(el, renderer) {
                    this.el = el;
                    this.renderer = renderer;
                    this.ContentEdited = new core_1.EventEmitter();
                    this.isEditable = false;
                    this.hasValueChanged = false;
                }
                ContentEditable.prototype.onDoubleClick = function () {
                    console.log('I was double clicked');
                    // this.isEditable = (this.isEditable) ? false : true;
                    console.log("isEditable: " + this.isEditable);
                };
                ContentEditable.prototype.onBlur = function () {
                    console.log('I have been Lost Focus');
                    this.isEditable = false;
                };
                ContentEditable.prototype.onFocus = function () {
                    console.log('I have been focused');
                    this.isEditable = true;
                };
                ContentEditable.prototype.ngOnInit = function () {
                    this.CreateEventsForAttribute();
                };
                ContentEditable.prototype.CreateEventsForAttribute = function () {
                    this.CreateContentEditableEnabled();
                    this.CreateContentEditableDisabled();
                };
                ContentEditable.prototype.CreateContentEditableEnabled = function () {
                    var _this = this;
                    this.renderer.listen(this.el.nativeElement, 'click', function () {
                        // console.log("Entering Edit MOde");
                        _this.value = _this.el.nativeElement.innerText;
                        _this.renderer.setElementAttribute(_this.el.nativeElement, "contenteditable", "true");
                        // console.table(<LeadModel>this.model);
                        // console.log("Lead - Contact:  " + JSON.stringify(this.model.Contact));
                    });
                };
                /**
                 * Disables editing on Element and when blur occurs
                 * @example <caption>Example usage of method1.</caption>
                 * // returns 2
                 * globalNS.method1(5, 10);
                 * @returns {Number} Returns the value of x for the equation.
                 */
                ContentEditable.prototype.CreateContentEditableDisabled = function () {
                    var _this = this;
                    this.renderer.listen(this.el.nativeElement, 'blur', function () {
                        _this.renderer.setElementAttribute(_this.el.nativeElement, "contenteditable", "false");
                        var modifiedValue = _this.el.nativeElement.innerText;
                        console.log(_this.value + " === " + modifiedValue);
                        _this.hasValueChanged = (_this.value === modifiedValue) ? false : true;
                        console.log("Model: " + JSON.stringify(_this.model));
                        console.log("Modified Value: " + modifiedValue);
                        if (_this.hasValueChanged) {
                            _this.model[_this.mpn] = _this.el.nativeElement.innerText;
                            console.log("Revised: Lead - Contact:  " + JSON.stringify(_this.model));
                            // this.cds.Save(this.serviceName, this.model);
                            _this.ContentEdited.emit(new ContentEditableResult_1.ContentEditableResult(_this, modifiedValue, _this.mpn));
                        }
                        else if (_this.serviceName === "contactDetail") {
                            // this.cds.Save(this.serviceName, this.model);
                            _this.ContentEdited.emit(new ContentEditableResult_1.ContentEditableResult(_this, modifiedValue, _this.mpn));
                        }
                    });
                };
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", core_1.EventEmitter)
                ], ContentEditable.prototype, "ContentEdited", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Object)
                ], ContentEditable.prototype, "model", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], ContentEditable.prototype, "mpn", void 0);
                __decorate([
                    core_1.HostListener('dblclick'),
                    __metadata("design:type", Function),
                    __metadata("design:paramtypes", []),
                    __metadata("design:returntype", void 0)
                ], ContentEditable.prototype, "onDoubleClick", null);
                __decorate([
                    core_1.HostBinding('class.editable-text'),
                    __metadata("design:type", Object)
                ], ContentEditable.prototype, "isEditable", void 0);
                __decorate([
                    core_1.HostListener('blur'),
                    __metadata("design:type", Function),
                    __metadata("design:paramtypes", []),
                    __metadata("design:returntype", void 0)
                ], ContentEditable.prototype, "onBlur", null);
                __decorate([
                    core_1.HostListener('focus'),
                    __metadata("design:type", Function),
                    __metadata("design:paramtypes", []),
                    __metadata("design:returntype", void 0)
                ], ContentEditable.prototype, "onFocus", null);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], ContentEditable.prototype, "serviceName", void 0);
                ContentEditable = __decorate([
                    core_1.Directive({
                        selector: "[sfm-editable]"
                    }),
                    __metadata("design:paramtypes", [core_1.ElementRef,
                        core_1.Renderer])
                ], ContentEditable);
                return ContentEditable;
            }());
            exports_1("ContentEditable", ContentEditable);
        }
    };
});
//# sourceMappingURL=ContentEditable.js.map