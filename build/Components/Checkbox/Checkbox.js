System.register(["@angular/core", "@angular/forms"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, forms_1, CheckhoxControl_VALUE_ACCESSOR, Checkbox;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            }
        ],
        execute: function () {
            exports_1("CheckhoxControl_VALUE_ACCESSOR", CheckhoxControl_VALUE_ACCESSOR = {
                provide: forms_1.NG_VALUE_ACCESSOR,
                useExisting: core_1.forwardRef(function () { return Checkbox; }),
                multi: true
            });
            Checkbox = /** @class */ (function () {
                function Checkbox() {
                    this.checked = false;
                    this.checkBoxState = "notChecked";
                    this.innerValue = false;
                    this.checkedValue = false;
                    this.disabled = false;
                    this.change = new core_1.EventEmitter();
                }
                Checkbox.prototype.onTouchedCallback = function () {
                };
                Checkbox.prototype.onChangeCallback = function (_) {
                    this.change.emit(_);
                };
                // From ControlValueAccessor interface
                Checkbox.prototype.writeValue = function (value) {
                    if (value !== this.innerValue) {
                        this.innerValue = value;
                        this.onChangeCallback(value);
                    }
                };
                // From ControlValueAccessor interface
                Checkbox.prototype.registerOnChange = function (fn) {
                    this.onChangeCallback = fn;
                };
                // From ControlValueAccessor interface
                Checkbox.prototype.registerOnTouched = function (fn) {
                    this.onTouchedCallback = fn;
                };
                Checkbox.prototype.ngOnInit = function () {
                };
                Checkbox.prototype.ngAfterViewInit = function () {
                    var _this = this;
                    setTimeout(function () {
                        _this.writeValue((_this.checkedValue === undefined) ? false : _this.checkedValue);
                    }, 1000);
                };
                Checkbox.prototype.onCheckboxClick = function () {
                    this.checked = (this.checked) ? false : true;
                    this.writeValue(this.checked);
                    this.toggleCheckboxState();
                };
                Checkbox.prototype.toggleCheckboxState = function () {
                    this.checkBoxState = (this.checkBoxState === "checked") ? "notChecked" : "checked";
                };
                Checkbox.prototype.setDisabledState = function (isDisabled) { };
                Object.defineProperty(Checkbox.prototype, "value", {
                    get: function () {
                        return this.innerValue;
                    },
                    enumerable: true,
                    configurable: true
                });
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Object)
                ], Checkbox.prototype, "checkedValue", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Object)
                ], Checkbox.prototype, "disabled", void 0);
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", core_1.EventEmitter)
                ], Checkbox.prototype, "change", void 0);
                Checkbox = __decorate([
                    core_1.Component({
                        selector: "sfm-checkbox",
                        moduleId: __moduleName,
                        templateUrl: "./checkbox.html",
                        styleUrls: [
                            "./checkbox.css"
                        ]
                    }),
                    __metadata("design:paramtypes", [])
                ], Checkbox);
                return Checkbox;
            }());
            exports_1("Checkbox", Checkbox);
        }
    };
});
//# sourceMappingURL=Checkbox.js.map