System.register(["@angular/core", "rxjs/Rx", "./FlyOutMenuEvents", "../../Services/index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, Rx_1, FlyOutMenuEvents_1, index_1, FlyoutMenuService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Rx_1_1) {
                Rx_1 = Rx_1_1;
            },
            function (FlyOutMenuEvents_1_1) {
                FlyOutMenuEvents_1 = FlyOutMenuEvents_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }
        ],
        execute: function () {
            FlyoutMenuService = /** @class */ (function () {
                function FlyoutMenuService(windowRef) {
                    this.windowRef = windowRef;
                    this.onMenuEvent$ = new Rx_1.default.Subject();
                    this.menuShown = true;
                    this.window = this.windowRef.GetNativeWindow();
                }
                FlyoutMenuService.prototype.OpenMenu = function () {
                    this.menuShown = true;
                    this.onMenuEvent$.next({
                        EventType: FlyOutMenuEvents_1.FlyOutMenuEvents.OPEN_MENU,
                        EventData: null
                    });
                };
                FlyoutMenuService.prototype.CloseMenu = function () {
                    this.menuShown = false;
                    this.onMenuEvent$.next({ EventType: FlyOutMenuEvents_1.FlyOutMenuEvents.CLOSE_MENU, EventData: null });
                };
                FlyoutMenuService.prototype.ToggleMenu = function () {
                    this.menuShown = (this.menuShown) ? false : true;
                };
                FlyoutMenuService.prototype.NavigateTo = function (url) {
                    this.ToggleMenu();
                    this.window
                        .open(url);
                };
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", Rx_1.default.Subject)
                ], FlyoutMenuService.prototype, "onMenuEvent$", void 0);
                FlyoutMenuService = __decorate([
                    core_1.Injectable(),
                    __metadata("design:paramtypes", [index_1.WindowRef])
                ], FlyoutMenuService);
                return FlyoutMenuService;
            }());
            exports_1("FlyoutMenuService", FlyoutMenuService);
        }
    };
});
//# sourceMappingURL=FlyoutMenuService.js.map