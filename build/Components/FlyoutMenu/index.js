System.register(["./FlyoutMenu", "./FlyOutMenuEvents", "./MenuItem", "./FlyoutMenuService"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (FlyoutMenu_1_1) {
                exportStar_1(FlyoutMenu_1_1);
            },
            function (FlyOutMenuEvents_1_1) {
                exportStar_1(FlyOutMenuEvents_1_1);
            },
            function (MenuItem_1_1) {
                exportStar_1(MenuItem_1_1);
            },
            function (FlyoutMenuService_1_1) {
                exportStar_1(FlyoutMenuService_1_1);
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map