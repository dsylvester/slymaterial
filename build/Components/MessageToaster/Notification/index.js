System.register(["./NotificationComponent", "./NotificationType", "./Notification"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (NotificationComponent_1_1) {
                exportStar_1(NotificationComponent_1_1);
            },
            function (NotificationType_1_1) {
                exportStar_1(NotificationType_1_1);
            },
            function (Notification_1_1) {
                exportStar_1(Notification_1_1);
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map