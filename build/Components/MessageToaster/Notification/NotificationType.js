System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var NotificationType;
    return {
        setters: [],
        execute: function () {
            (function (NotificationType) {
                NotificationType[NotificationType["INFO"] = 100] = "INFO";
                NotificationType[NotificationType["SUCCESS"] = 200] = "SUCCESS";
                NotificationType[NotificationType["WARNING"] = 300] = "WARNING";
                NotificationType[NotificationType["ERROR"] = 500] = "ERROR";
            })(NotificationType || (NotificationType = {}));
            exports_1("NotificationType", NotificationType);
        }
    };
});
//# sourceMappingURL=NotificationType.js.map