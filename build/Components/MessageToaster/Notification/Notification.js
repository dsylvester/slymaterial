System.register(["./NotificationType"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var NotificationType_1, Notification;
    return {
        setters: [
            function (NotificationType_1_1) {
                NotificationType_1 = NotificationType_1_1;
            }
        ],
        execute: function () {
            Notification = /** @class */ (function () {
                function Notification(Message, NoticeType, Details, NoticeIcon, id) {
                    if (NoticeIcon === void 0) { NoticeIcon = "flaticon-sign"; }
                    if (id === void 0) { id = null; }
                    this.Message = Message;
                    this.NoticeType = NoticeType;
                    this.Details = Details;
                    this.NoticeIcon = NoticeIcon;
                    this.id = id;
                    this.NoticeCssClass = "";
                    this.setCssClass();
                    console.log('Notice CSS Class: : ' + this.NoticeCssClass);
                    if (id === null) {
                        id = Notification.id;
                    }
                    Notification.id++;
                    console.log("id: " + id);
                    console.log("Notification ID: " + Notification.id);
                    this.setDefaultIconForError();
                }
                Notification.prototype.setCssClass = function () {
                    switch (this.NoticeType) {
                        case NotificationType_1.NotificationType.SUCCESS:
                            this.NoticeCssClass = "toasterSuccessMessage";
                            break;
                        case NotificationType_1.NotificationType.ERROR:
                            this.NoticeCssClass = "toasterErrorMessage";
                            break;
                        case NotificationType_1.NotificationType.WARNING:
                            this.NoticeCssClass = "toasterWarningMessage";
                            break;
                        case NotificationType_1.NotificationType.INFO:
                            this.NoticeCssClass = "toasterInfoMessage";
                            break;
                    }
                };
                Notification.prototype.setDefaultIconForError = function () {
                    if (this.NoticeType === NotificationType_1.NotificationType.ERROR && this.NoticeIcon === "flaticon-sign") {
                        this.NoticeIcon = "fa fa-exclamation-triangle fa-2x";
                    }
                };
                Notification.id = 1;
                return Notification;
            }());
            exports_1("Notification", Notification);
        }
    };
});
//# sourceMappingURL=Notification.js.map