System.register(["./messageToaster", "./Services/index", "./Notification/index"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var exportedNames_1 = {
        "MessageToaster": true,
        "MessageToasterService": true
    };
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default" && !exportedNames_1.hasOwnProperty(n)) exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (messageToaster_1_1) {
                exports_1({
                    "MessageToaster": messageToaster_1_1["MessageToaster"]
                });
            },
            function (index_1_1) {
                exports_1({
                    "MessageToasterService": index_1_1["MessageToasterService"]
                });
            },
            function (index_2_1) {
                exportStar_1(index_2_1);
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map