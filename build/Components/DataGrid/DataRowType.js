System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DataRowType;
    return {
        setters: [],
        execute: function () {
            (function (DataRowType) {
                DataRowType[DataRowType["HTML"] = 0] = "HTML";
                DataRowType[DataRowType["COMPONENT"] = 1] = "COMPONENT";
                DataRowType[DataRowType["INLINE"] = 2] = "INLINE";
            })(DataRowType || (DataRowType = {}));
            exports_1("DataRowType", DataRowType);
        }
    };
});
//# sourceMappingURL=DataRowType.js.map