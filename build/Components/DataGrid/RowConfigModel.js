System.register(["./DataRowType"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DataRowType_1, RowConfigModel;
    return {
        setters: [
            function (DataRowType_1_1) {
                DataRowType_1 = DataRowType_1_1;
            }
        ],
        execute: function () {
            RowConfigModel = /** @class */ (function () {
                function RowConfigModel(Name, Size, OrdinalName, RowType) {
                    if (RowType === void 0) { RowType = DataRowType_1.DataRowType.HTML; }
                    this.Name = Name;
                    this.Size = Size;
                    this.OrdinalName = OrdinalName;
                    this.RowType = RowType;
                }
                return RowConfigModel;
            }());
            exports_1("RowConfigModel", RowConfigModel);
        }
    };
});
//# sourceMappingURL=RowConfigModel.js.map