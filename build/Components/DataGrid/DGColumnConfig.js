System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DGColumnConfig;
    return {
        setters: [],
        execute: function () {
            DGColumnConfig = /** @class */ (function () {
                function DGColumnConfig(Name, Size) {
                    if (Size === void 0) { Size = 1; }
                    this.Name = Name;
                    this.Size = Size;
                    if (this.Name.length === 0) {
                        throw new Error("You must provide a Name for DGColumnConfig()");
                    }
                }
                return DGColumnConfig;
            }());
            exports_1("DGColumnConfig", DGColumnConfig);
        }
    };
});
//# sourceMappingURL=DGColumnConfig.js.map