System.register(["@angular/core", "rxjs/Subject", "./index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, Subject_1, index_1, DGDataRow;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (Subject_1_1) {
                Subject_1 = Subject_1_1;
            },
            function (index_1_1) {
                index_1 = index_1_1;
            }
        ],
        execute: function () {
            DGDataRow = /** @class */ (function () {
                function DGDataRow(dataGridService, resolver) {
                    this.dataGridService = dataGridService;
                    this.resolver = resolver;
                    this.rowSelected = false;
                    this.useSelectAllRows = false;
                    this.selectedRows = new Map();
                    this.FilteredData = new Array();
                    this.onRowSelected = new Subject_1.Subject();
                }
                DGDataRow.prototype.ngOnInit = function () {
                };
                DGDataRow.prototype.ngAfterViewInit = function () {
                    var _this = this;
                    // console.log(`This.rowConfig: ${JSON.stringify(this.rowConfig, null, 5)}`);
                    // console.log(`This.Data: ${JSON.stringify(this.data, null, 5)}`);
                    console.log(this.data);
                    if (this.data !== undefined) {
                        setTimeout(function () {
                            //  this.populateSelectedRows(this.data);
                            _this.FilteredData = _this.data.map(function (x) {
                                return x;
                            });
                            console.log(_this.FilteredData);
                        }, 1000);
                    }
                };
                DGDataRow.prototype.ngAfterContentInit = function () {
                    // this.parent.CreateComponent(this.resolver.ComponentFactoryResolver(Chip));
                };
                DGDataRow.prototype.SetMaxSize = function (colNum) {
                    return (colNum * 10) + "%";
                };
                DGDataRow.prototype.RowClicked = function (idx) {
                    this.ToggleRowSelection(idx);
                };
                DGDataRow.prototype.ToggleRowSelection = function (idx) {
                    try {
                        var currentValue = this.selectedRows.get(idx);
                        this.selectedRows.set(idx, (currentValue) ? false : true);
                        // console.log(`aaaa: ${this.selectedRows[idx]}`);
                    }
                    catch (error) {
                        throw new Error("Unable to find provided Index for Selected Row");
                    }
                };
                DGDataRow.prototype.isRowSelect = function (idx) {
                    var val = this.selectedRows.get(idx);
                    if (val === undefined) {
                        return false;
                        // throw new Error("Can't determine row index");
                    }
                    return val;
                };
                DGDataRow.prototype.CreateComponent = function () {
                };
                DGDataRow.prototype.populateSelectedRows = function (data) {
                    var _this = this;
                    console.log("data: " + JSON.stringify(data, null, 5));
                    if (data === undefined || data.length === 0) {
                        return;
                    }
                    var index = 0;
                    data.forEach(function () {
                        _this.selectedRows.set(index, false);
                        index++;
                    });
                };
                DGDataRow.prototype.RowHasBeenDoubledClick = function (data) {
                    this.onRowSelected.next(data);
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Array)
                ], DGDataRow.prototype, "data", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Array)
                ], DGDataRow.prototype, "rowConfig", void 0);
                __decorate([
                    core_1.Output(),
                    __metadata("design:type", Subject_1.Subject)
                ], DGDataRow.prototype, "onRowSelected", void 0);
                __decorate([
                    core_1.ViewChild('customComponent', { read: core_1.ViewContainerRef }),
                    __metadata("design:type", Object)
                ], DGDataRow.prototype, "parent", void 0);
                DGDataRow = __decorate([
                    core_1.Component({
                        selector: "sfm-data-grid-row",
                        moduleId: __moduleName,
                        templateUrl: "./dgDataRow.html",
                        styleUrls: ["./dataGrid.css", "DGDataRow.css"]
                    }),
                    __metadata("design:paramtypes", [index_1.DataGridService,
                        core_1.ComponentFactoryResolver])
                ], DGDataRow);
                return DGDataRow;
            }());
            exports_1("DGDataRow", DGDataRow);
        }
    };
});
//# sourceMappingURL=DGDataRow.js.map