System.register(["@angular/core", "@angular/forms"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, forms_1, CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, ToggleSwitch;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            }
        ],
        execute: function () {
            exports_1("CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR", CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
                provide: forms_1.NG_VALUE_ACCESSOR,
                useExisting: core_1.forwardRef(function () { return ToggleSwitch; }),
                multi: true
            });
            ToggleSwitch = /** @class */ (function () {
                function ToggleSwitch() {
                    this.OnText = "true";
                    this.OffText = "false";
                    this.Indicator = false;
                    this.innerValue = false;
                }
                Object.defineProperty(ToggleSwitch.prototype, "value", {
                    get: function () {
                        return this.innerValue;
                    },
                    // set accessor including call the onchange callback
                    set: function (v) {
                        if (v !== this.innerValue) {
                            this.innerValue = v;
                            this.onChangeCallback(v);
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                // Set touched on blur
                ToggleSwitch.prototype.onBlur = function () {
                    this.onTouchedCallback();
                };
                // From ControlValueAccessor interface
                ToggleSwitch.prototype.writeValue = function (value) {
                    if (value !== this.innerValue) {
                        this.innerValue = value;
                    }
                };
                // From ControlValueAccessor interface
                ToggleSwitch.prototype.registerOnChange = function (fn) {
                    this.onChangeCallback = fn;
                };
                // From ControlValueAccessor interface
                ToggleSwitch.prototype.registerOnTouched = function (fn) {
                    this.onTouchedCallback = fn;
                };
                ToggleSwitch.prototype.Toggle = function () {
                    this.Indicator = (this.Indicator === false ? true : false);
                    if (this.Indicator) {
                        console.log(this.tg);
                        this.tg.nativeElement.style.left = "15px";
                        this.tg.nativeElement.style.background = "#919191";
                        this.tc.nativeElement.style.background = "#C8C8C8";
                    }
                    else {
                        this.tg.nativeElement.style.left = "";
                        this.tg.nativeElement.style.background = "";
                        this.tc.nativeElement.style.background = "";
                    }
                    this.value = this.Indicator;
                };
                ToggleSwitch.prototype.ngOnInit = function () {
                    this.value = this.Indicator;
                };
                __decorate([
                    core_1.ViewChild('toggleContainer'),
                    __metadata("design:type", Object)
                ], ToggleSwitch.prototype, "tc", void 0);
                __decorate([
                    core_1.ViewChild('toggle'),
                    __metadata("design:type", Object)
                ], ToggleSwitch.prototype, "tg", void 0);
                __decorate([
                    core_1.Input("on-text"),
                    __metadata("design:type", Object)
                ], ToggleSwitch.prototype, "OnText", void 0);
                __decorate([
                    core_1.Input("off-text"),
                    __metadata("design:type", Object)
                ], ToggleSwitch.prototype, "OffText", void 0);
                ToggleSwitch = __decorate([
                    core_1.Component({
                        moduleId: __moduleName,
                        selector: "sfm-toggle",
                        templateUrl: "./toggleSwitch.html",
                        providers: [Location, CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
                    }),
                    __metadata("design:paramtypes", [])
                ], ToggleSwitch);
                return ToggleSwitch;
            }());
            exports_1("ToggleSwitch", ToggleSwitch);
        }
    };
});
//# sourceMappingURL=toggleSwitch.js.map