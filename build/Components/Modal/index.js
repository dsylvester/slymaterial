System.register(["./Modal", "./ModalService"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (Modal_1_1) {
                exports_1({
                    "Modal": Modal_1_1["Modal"]
                });
            },
            function (ModalService_1_1) {
                exports_1({
                    "ModalService": ModalService_1_1["ModalService"]
                });
            }
        ],
        execute: function () {
        }
    };
});
//# sourceMappingURL=index.js.map