System.register(["@angular/core", "./DialogService"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, DialogService_1, Dialog;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (DialogService_1_1) {
                DialogService_1 = DialogService_1_1;
            }
        ],
        execute: function () {
            Dialog = /** @class */ (function () {
                function Dialog(dialogService, cfr) {
                    this.dialogService = dialogService;
                    this.cfr = cfr;
                    this.hasError = false;
                    this.ShowModal = false;
                    console.log("Dialog Loaded!");
                    this.dialogService.ToggleModal();
                    console.log("ShowModal: " + this.dialogService.ShowModal);
                }
                Dialog.prototype.ngOnInit = function () {
                };
                Dialog.prototype.ngAfterViewInit = function () {
                    console.log(this.modalContainer);
                    this.dialogService.modalWrapper = this.modalContainer;
                };
                __decorate([
                    core_1.ViewChild("modalWrapper", { read: core_1.ViewContainerRef }),
                    __metadata("design:type", core_1.ViewContainerRef)
                ], Dialog.prototype, "modalContainer", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Object)
                ], Dialog.prototype, "ShowModal", void 0);
                Dialog = __decorate([
                    core_1.Component({
                        selector: "sfm-dialog",
                        moduleId: __moduleName,
                        templateUrl: "./Dialog.html",
                        styleUrls: ["./Dialog.css"],
                        host: {
                            class: "dialogContainer"
                        },
                        providers: [Location],
                        animations: []
                    }),
                    __metadata("design:paramtypes", [DialogService_1.DialogService, core_1.ComponentFactoryResolver])
                ], Dialog);
                return Dialog;
            }());
            exports_1("Dialog", Dialog);
        }
    };
});
//# sourceMappingURL=Dialog.js.map