System.register(["@angular/core", "@angular/forms", "tsg-calendar-lib"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, forms_1, tsg_calendar_lib_1, CalendarControl_VALUE_ACCESSOR, CalendarControl;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (tsg_calendar_lib_1_1) {
                tsg_calendar_lib_1 = tsg_calendar_lib_1_1;
            }
        ],
        execute: function () {
            exports_1("CalendarControl_VALUE_ACCESSOR", CalendarControl_VALUE_ACCESSOR = {
                provide: forms_1.NG_VALUE_ACCESSOR,
                useExisting: core_1.forwardRef(function () { return CalendarControl; }),
                multi: true
            });
            CalendarControl = /** @class */ (function () {
                function CalendarControl() {
                    this.innerValue = new Date();
                    this.yearSelectorIndex = 0;
                    this.hasYearChanged = false;
                    this.CalendarDate = null;
                    this.CalendarShown = false;
                    this.YearSelectorShown = false;
                    this.selectedDay = -1;
                    this.selectedYear = -1;
                    this.SelectableYears = new Array();
                    this.previousYears = 20;
                    this.futureYears = 20;
                    this.onChange = new core_1.EventEmitter();
                    var cDate = new Date();
                    var year = cDate.getFullYear();
                    var month = cDate.getMonth() + 1;
                    this.Cal = new tsg_calendar_lib_1.Calendar(year, month);
                }
                Object.defineProperty(CalendarControl.prototype, "value", {
                    get: function () {
                        return this.innerValue;
                    },
                    // set accessor including call the onchange callback
                    set: function (v) {
                        if (v !== this.innerValue) {
                            this.innerValue = v;
                            this.CalendarDate = v;
                            this.onChangeCallback(v);
                            if (this.hasYearChanged && this.yearSelectorIndex !== 0) {
                                this.onChange.next(v);
                                this.hasYearChanged = false;
                                this.yearSelectorIndex = 0;
                            }
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                // Set touched on blur
                CalendarControl.prototype.onBlur = function () {
                    this.onTouchedCallback();
                };
                // From ControlValueAccessor interface
                CalendarControl.prototype.writeValue = function (value) {
                    if (value !== this.innerValue) {
                        this.innerValue = value;
                    }
                };
                // From ControlValueAccessor interface
                CalendarControl.prototype.registerOnChange = function (fn) {
                    this.onChangeCallback = fn;
                };
                // From ControlValueAccessor interface
                CalendarControl.prototype.registerOnTouched = function (fn) {
                    this.onTouchedCallback = fn;
                };
                CalendarControl.prototype.onTouchedCallback = function () { };
                CalendarControl.prototype.onChangeCallback = function (_) { };
                CalendarControl.prototype.SelectDate = function (day, el) {
                    // console.log(el.nativeElement);
                    this.selectedDay = day;
                    // console.log(`this.SelectedDay: ${this.selectedDay}`);
                    if (day === 0) {
                        return null;
                    }
                    this.value = new Date(this.Cal.currentYear, this.Cal.currentMonth, day);
                    this.innerValue = this.value;
                    // console.log("Selected Date: " + this.value.toDateString());
                    // console.log(`${this.Cal.currentYear}-${this.Cal.currentMonth + 1}-${day}`);
                    this.CloseCalendar();
                    return this.value;
                };
                CalendarControl.prototype.NextMonth = function () {
                    var currentMonth = this.Cal.currentMonth + 1;
                    var currentYear = this.Cal.currentYear;
                    currentMonth++;
                    // console.log("Current Month Value: " + currentMonth);
                    if (currentMonth > 12) {
                        currentMonth = 1;
                        this.Cal.currentYear++;
                        currentYear = this.Cal.currentYear;
                    }
                    // console.log(`Current Month ${currentMonth} \n Current Year ${currentYear}`);
                    this.Cal = new tsg_calendar_lib_1.Calendar(currentYear, currentMonth);
                    // console.log(this.Cal);
                };
                CalendarControl.prototype.PreviousMonth = function () {
                    var currentMonth = this.Cal.currentMonth; // Increase by 1 for 0 based Index
                    var currentYear = this.Cal.currentYear;
                    currentMonth--;
                    if (currentMonth < 0) {
                        currentMonth = 11;
                        this.Cal.currentYear--;
                        currentYear = this.Cal.currentYear;
                    }
                    // // console.log(`Current Month ${currentMonth} \n Current Year ${currentYear}`);
                    this.Cal = new tsg_calendar_lib_1.Calendar(currentYear, currentMonth + 1);
                    // // console.log(this.Cal);
                };
                CalendarControl.prototype.ngOnInit = function () {
                    if (this.InitialValue !== undefined) {
                        this.value = this.InitialValue;
                        var tempDate = new Date(this.InitialValue);
                        // console.log(`TempDate: ${tempDate}`);
                        // console.log(`Calendar Date: Year: ${tempDate.getFullYear()}  
                        // Month: ${tempDate.getMonth()} `);
                        this.Cal = new tsg_calendar_lib_1.Calendar(tempDate.getFullYear(), tempDate.getMonth());
                    }
                };
                CalendarControl.prototype.ngAfterViewInit = function () {
                    this.populateSelectableYears(this.Cal.currentYear);
                };
                CalendarControl.prototype.onCalendarClick = function () {
                    this.ShowCalendar();
                };
                CalendarControl.prototype.ShowCalendar = function () {
                    this.CalendarShown = true;
                };
                CalendarControl.prototype.CloseCalendar = function () {
                    this.CalendarShown = false;
                };
                CalendarControl.prototype.showYearSelector = function () {
                    this.ToggleYearSelector();
                };
                CalendarControl.prototype.ToggleYearSelector = function () {
                    this.YearSelectorShown = (this.YearSelectorShown) ? false : true;
                };
                CalendarControl.prototype.isSelectedDay = function (day) {
                    var currentDay = parseInt(day);
                };
                CalendarControl.prototype.calendarYearChanged = function (year) {
                    // Set the Year to newly Selected Year
                    this.Cal.currentYear = parseInt(year);
                    this.ToggleYearSelector();
                    this.setActiveDate(this.Cal);
                    this.hasYearChanged = true;
                };
                CalendarControl.prototype.setActiveDate = function (cal) {
                    this.Cal = new tsg_calendar_lib_1.Calendar(cal.currentYear, (cal.currentMonth + 1));
                };
                CalendarControl.prototype.populateSelectableYears = function (year) {
                    for (var i = 0; i < this.previousYears; i++) {
                        this.SelectableYears.push(year - i);
                    }
                    for (var i = 0; i < this.futureYears; i++) {
                        this.SelectableYears.push(year + i);
                    }
                    var dup = this.SelectableYears.findIndex(function (x) {
                        return x = year;
                    });
                    if (dup !== -1) {
                        this.SelectableYears.splice(dup, 1);
                    }
                    this.SelectableYears.sort(function (x, y) {
                        return x - y;
                    });
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], CalendarControl.prototype, "InitialValue", void 0);
                __decorate([
                    core_1.Input('previous-years'),
                    __metadata("design:type", Object)
                ], CalendarControl.prototype, "previousYears", void 0);
                __decorate([
                    core_1.Input('future-years'),
                    __metadata("design:type", Object)
                ], CalendarControl.prototype, "futureYears", void 0);
                __decorate([
                    core_1.Output('change'),
                    __metadata("design:type", core_1.EventEmitter)
                ], CalendarControl.prototype, "onChange", void 0);
                CalendarControl = __decorate([
                    core_1.Component({
                        selector: "sfm-calendar",
                        moduleId: __moduleName,
                        templateUrl: "./index.html",
                        providers: [CalendarControl_VALUE_ACCESSOR]
                    }),
                    __metadata("design:paramtypes", [])
                ], CalendarControl);
                return CalendarControl;
            }());
            exports_1("CalendarControl", CalendarControl);
        }
    };
});
//# sourceMappingURL=CalendarControl.js.map