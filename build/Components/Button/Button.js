System.register(["@angular/core", "./ButtonSize"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ButtonSize_1, Button;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ButtonSize_1_1) {
                ButtonSize_1 = ButtonSize_1_1;
            }
        ],
        execute: function () {
            Button = /** @class */ (function () {
                function Button() {
                    this.buttonSize = 75;
                }
                Button.prototype.ngOnInit = function () {
                };
                Button.prototype.ngAfterViewInit = function () {
                    var _this = this;
                    setTimeout(function () {
                        _this.determinButtonSize(_this.Size);
                        console.log("ButtonSize: " + _this.buttonSize);
                    }, 1000);
                };
                Button.prototype.determinButtonSize = function (size) {
                    this.buttonSize = (ButtonSize_1.ButtonSize.get(size) === undefined) ?
                        ButtonSize_1.ButtonSize.get("sm") : ButtonSize_1.ButtonSize.get(size);
                };
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", Boolean)
                ], Button.prototype, "Disabled", void 0);
                __decorate([
                    core_1.Input(),
                    __metadata("design:type", String)
                ], Button.prototype, "Size", void 0);
                Button = __decorate([
                    core_1.Component({
                        selector: "sfm-button",
                        moduleId: __moduleName,
                        templateUrl: "./Button.html",
                        styleUrls: ["./Buttons.css"],
                        providers: [Location],
                        animations: [
                            core_1.trigger('marqueeOpening', [
                                core_1.state('1', core_1.style({
                                    'height': '38px',
                                    'line-height': '38px',
                                    'width': '100%',
                                    'opacity': 1
                                })),
                                core_1.state('2', core_1.style({
                                    'height': '0px',
                                    'line-height': '0px',
                                    'width': '0%',
                                    'opacity': .1
                                })),
                                core_1.transition('1 => 2', core_1.animate('500ms ease-in')),
                                core_1.transition('2 => 1', core_1.animate('100ms ease-out'))
                            ])
                        ]
                    }),
                    __metadata("design:paramtypes", [])
                ], Button);
                return Button;
            }());
            exports_1("Button", Button);
        }
    };
});
//# sourceMappingURL=Button.js.map