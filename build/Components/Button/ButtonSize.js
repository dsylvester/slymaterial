System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var buttonSize, ButtonSize;
    return {
        setters: [],
        execute: function () {
            buttonSize = new Map();
            buttonSize.set("sm", 75);
            buttonSize.set("med", 150);
            buttonSize.set("lg", 175);
            buttonSize.set("xlg", 200);
            exports_1("ButtonSize", ButtonSize = buttonSize);
        }
    };
});
//# sourceMappingURL=ButtonSize.js.map