import { task, watch } from 'gulp';
import { AngularComponentsHtmlFiles, AngularHtmlCopyTask,
         AngularComponentLessTask, AngularComponentsLessFiles } from "../constants";

task('watch', () => {
    watch([AngularComponentsHtmlFiles], [AngularHtmlCopyTask]);
    watch([AngularComponentsLessFiles], [AngularComponentLessTask]);
});
