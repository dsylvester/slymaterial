"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const gulp_1 = require("gulp");
const constants_1 = require("../constants");
gulp_1.task('watch', () => {
    gulp_1.watch([constants_1.AngularComponentsHtmlFiles], [constants_1.AngularHtmlCopyTask]);
    gulp_1.watch([constants_1.AngularComponentsLessFiles], [constants_1.AngularComponentLessTask]);
});
//# sourceMappingURL=watch.js.map