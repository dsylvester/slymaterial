import { task } from 'gulp';
import { AngularHtmlCopyTask, WatchTask, AngularComponentLessTask } from "../constants";

task('default', [ AngularHtmlCopyTask, AngularComponentLessTask, WatchTask ]);
