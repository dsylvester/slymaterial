import * as plumber from 'gulp-plumber';
import * as sourcemaps from "gulp-sourcemaps";
import * as less from "gulp-less";
import * as autoprefixer from "gulp-autoprefixer";
import { task, src, dest} from 'gulp';

import { DestDir, AngularComponentsHtmlFiles, AngularHtmlCopyTask, 
         AngularComponentsLessFiles, AngularComponentLessCopyTask,
         AngularComponentLessTask } from "../constants";

task(AngularHtmlCopyTask, () => {
console.log(AngularComponentsHtmlFiles);
    src(AngularComponentsHtmlFiles)
        .pipe(dest(DestDir));
});



task(AngularComponentLessTask, () => {

    src(AngularComponentsLessFiles)
        .pipe(plumber({}))
        .pipe(sourcemaps.init())
        .pipe(less({}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(DestDir));


        
});

task(AngularComponentLessTask, () => {

    src(AngularComponentsLessFiles)
        .pipe(plumber({}))
        .pipe(sourcemaps.init())
        .pipe(less({}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(DestDir));


        
});



