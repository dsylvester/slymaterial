"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const less = require("gulp-less");
const autoprefixer = require("gulp-autoprefixer");
const gulp_1 = require("gulp");
const constants_1 = require("../constants");
gulp_1.task(constants_1.AngularHtmlCopyTask, () => {
    console.log(constants_1.AngularComponentsHtmlFiles);
    gulp_1.src(constants_1.AngularComponentsHtmlFiles)
        .pipe(gulp_1.dest(constants_1.DestDir));
});
gulp_1.task(constants_1.AngularComponentLessTask, () => {
    gulp_1.src(constants_1.AngularComponentsLessFiles)
        .pipe(plumber({}))
        .pipe(sourcemaps.init())
        .pipe(less({}))
        .pipe(autoprefixer({
        browsers: ['last 2 versions']
    }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp_1.dest(constants_1.DestDir));
});
gulp_1.task(constants_1.AngularComponentLessTask, () => {
    gulp_1.src(constants_1.AngularComponentsLessFiles)
        .pipe(plumber({}))
        .pipe(sourcemaps.init())
        .pipe(less({}))
        .pipe(autoprefixer({
        browsers: ['last 2 versions']
    }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp_1.dest(constants_1.DestDir));
});
//# sourceMappingURL=components.js.map