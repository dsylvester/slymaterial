"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RootDir = "./";
exports.SrcDir = "./src";
exports.TsRoot = "./src";
exports.DestDir = "./dist/";
exports.BuildDir = "./build/";
exports.BundleDir = "./bundle";
exports.AngularComponentsHtmlFiles = `${exports.TsRoot}/**/*.html`;
exports.AngularComponentsLessFiles = `${exports.TsRoot}/**/*.less`;
exports.AngularHtmlCopyTask = "AngularHtml.Copy";
exports.AngularComponentLessTask = "AngularComponentLess";
exports.AngularComponentLessCopyTask = "AngularComponentLess.Copy";
exports.concatTask = "concat";
exports.UmdTask = "umd";
exports.BuildCopy = "BuildCopy";
exports.WatchTask = "watch";
exports.CleanBuildFolderTask = "cleanBuildFolder";
//# sourceMappingURL=constants.js.map