# README #


| Components   |      Name      |  Selector         |      Status   |
|--------------|:--------------:|------------------:|:-------------:|
|    Modal     |    Modal       | sfm-modal         |  In-Progress  |
|    Button    |    Button      | sfm-button        |  In-Progress  |
|    Dialog    |    Dialog      | sfm-dialog        |  In-Progress  |
| Fly Out Menu |    Menu        | sfm-flyout-menu   |  In-Progress  |
|    Marquee   |    Marquee     | sfm-marquee       |  In-Progress  |
|    Toaster   |    Toaster     | sfm-toaster       |  In-Progress  |
|    Toogle    |    Toogle      | sfm-toggle        |  In-Progress  |